﻿using AST.Infrastructure.Bussiness.Mvvm;

namespace AST.Infrastructure.Bussiness.ViewModels
{
    public class SelectableWorkspaceViewModel : ChainingViewModelBase
    {
        private ViewModelBase currentViewModel;
        public ViewModelBase CurrentViewModel
        {
            get => currentViewModel;
            set
            {
                currentViewModel = value;
                FirePropertyChanged(nameof(CurrentViewModel));
            }
        }
    }
}
