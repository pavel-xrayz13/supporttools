﻿using System.Data;

namespace AST.Infrastructure.Dal
{
    public interface IDbConnector
    {
        int DefaultCommandTimeout { get; }
        IDbConnection GetConnection();
    }
}
