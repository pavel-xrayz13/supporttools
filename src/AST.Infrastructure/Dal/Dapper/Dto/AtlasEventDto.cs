﻿using System;

namespace AST.Infrastructure.Dal.Dapper.Dto
{
    public class AtlasEventDto
    {
        public DateTime Created { get; set; }
        public int EventCode { get; set; }
        public byte[] EventData { get; set; }
        public string IsAlarm { get; set; }
    }
}
