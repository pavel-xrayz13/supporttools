﻿using System.Data;
using System.Linq;
using System.Configuration;
using MySql.Data.MySqlClient;

namespace AST.Infrastructure.Dal.MySql
{
    public class MySqlDbConnector : IDbConnector
    {
        private static readonly string AtlasConnectionStringName = "AtlasConnectionString";
        private static readonly string DefaultCommandTimeoutName = "DefaultCommandTimeout";
        private static readonly int DefaultCommandTimeoutValue = 60;

        private readonly string connectionString;

        public MySqlDbConnector()
        {
            connectionString = ConfigurationManager.ConnectionStrings[AtlasConnectionStringName].ConnectionString;
            InitializeCommandTimeout();
        }

        public int DefaultCommandTimeout { get; private set; }

        public IDbConnection GetConnection() => new MySqlConnection(connectionString);

        private void InitializeCommandTimeout()
        {
            var appSettings = ConfigurationManager.AppSettings;
            if (appSettings.AllKeys.Contains(DefaultCommandTimeoutName))
            {
                var timeoutString = appSettings[DefaultCommandTimeoutName];
                if (int.TryParse(timeoutString, out int result))
                    DefaultCommandTimeout = result;
                else
                    DefaultCommandTimeout = DefaultCommandTimeoutValue;
            }
            else
            {
                DefaultCommandTimeout = DefaultCommandTimeoutValue;
            }
        }
    }
}
