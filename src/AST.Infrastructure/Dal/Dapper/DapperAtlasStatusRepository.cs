﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AST.Infrastructure.Dal.Dapper.Dto;
using AST.Infrastructure.Domain.Entities;
using Dapper;

namespace AST.Infrastructure.Dal.Dapper
{
    public class DapperAtlasStatusRepository : IAtlasStatusRepository
    {
        private readonly IDbConnector connector;

        public DapperAtlasStatusRepository(IDbConnector connector)
        {
            this.connector = connector ?? throw new ArgumentNullException(nameof(connector));
        }

        public async Task<AtlasStatusEntity> GetAtlasStatusBySerialId(string serial, CancellationToken ct = default)
        {
            const string historySql = 
                "SELECT " +
                "dho1.doh_is_online AS DeviceStatus, " +
                "dho1.doh_created AS Created " +
                "FROM device_history_online dho1 " +
                "WHERE dho1.doh_dev_login = @DeviceSerial " +
                "ORDER BY dho1.doh_created DESC";

            const string statusSql = 
                "SELECT " +
                "d1.dev_id AS Id, " +
                "d1.dev_login AS DeviceId, " +
                "d1.dev_is_online AS DeviceStatus, " +
                "d1.dev_sim_type AS SimCardType, " +
                "d1.dev_sim_iccid AS Iccid, " +
                "d1.dev_sim_balance AS SimCardLastBalanceValue, " +
                "d1.dev_sim_refreshed AS SimCardLastBalanceCheck, " +
                "d1.dev_auth_equip AS AuthEquip, " +
                "d1.dev_auth_immo_pin AS AuthImmoPin, " +
                "d1.dev_auth_immo_tag AS AuthImmoTag, " +
                "d1.dev_auth_slave AS AuthSlave " +
                "FROM devices d1 " +
                "WHERE dev_login = @DeviceSerial";

            const string pairingDataSql =
                "SELECT " +
                "dev1.dev_id AS DeviceId, " +
                "p1.pair_u_id AS UserId, " +
                "p1.pair_created AS PairCreated, " +
                "u1.u_login AS UserLogin " +
                "FROM devices dev1 " +
                "LEFT JOIN pairs p1 ON dev1.dev_id = p1.pair_dev_id " +
                "LEFT JOIN users u1 ON u1.u_id = p1.pair_u_id " +
                "WHERE dev1.dev_id = @Id";

            using (var connection = connector.GetConnection())
            {
                var timeout = connector.DefaultCommandTimeout;
                var atlasStatusDto = await connection.QueryFirstOrDefaultAsync<AtlasStatusDto>(
                    new CommandDefinition(statusSql, new { DeviceSerial = serial }, cancellationToken: ct, commandTimeout: timeout)).ConfigureAwait(false);
                var history = await connection.QueryAsync<AtlasHistoryDto>(
                    new CommandDefinition(historySql, new { DeviceSerial = serial }, cancellationToken: ct, commandTimeout: timeout)).ConfigureAwait(false);

                AtlasPairingDto pairingDataDto = null;
                if (atlasStatusDto != null)
                {
                    pairingDataDto = await connection.QueryFirstOrDefaultAsync<AtlasPairingDto>(
                        new CommandDefinition(pairingDataSql, new { atlasStatusDto.Id }, cancellationToken: ct, commandTimeout: timeout)).ConfigureAwait(false);
                }

                return DtoConverters.CreateEntityFromDto(atlasStatusDto, pairingDataDto, history);
            }
        }

        public async Task<AtlasGeoDataEntity> GetAtlasGeoDataBySerialId(int id, CancellationToken ct = default)
        {
            const string geoDataSql =
                "SELECT " +
                "dc1.dc_latitude AS GeoDataLattitude, " +
                "dc1.dc_longitude AS GeoDataLongitude, " +
                "dc1.dc_created AS GeoDataCreated " +
                "FROM data_coordinates dc1 " +
                "WHERE dc1.dc_dev_id = @id " +
                "ORDER BY dc1.dc_created DESC LIMIT 1";

            using (var connection = connector.GetConnection())
            {
                var timeout = connector.DefaultCommandTimeout;
                var geoDataDto = await connection.QueryFirstOrDefaultAsync<AtlasGeoDataDto>(
                    new CommandDefinition(geoDataSql, new { id }, cancellationToken: ct, commandTimeout: timeout)).ConfigureAwait(false);

                return DtoConverters.CreateGeoDataFromDto(geoDataDto);
            }
        }

        public async Task<IEnumerable<AtlasEventEntity>> GetAtlasEventsByFilter(string serial, GetAtlasEventsFilter filter, CancellationToken ct = default)
        {
            const string eventsDataSql =
                "SELECT " +
                "de1.ev_created AS Created, " +
                "de1.ev_code AS EventCode, " +
                "de1.ev_data AS EventData, " +
                "de1.ev_is_alarm AS IsAlarm " +
                "FROM data_events de1 " +
                "WHERE de1.ev_dev_login = @DeviceSerial " +
                "AND de1.ev_created >= @StartDate " +
                "AND de1.ev_created <= @EndDate " +
                "ORDER BY de1.ev_created DESC";

            var inputs = new
            {
                DeviceSerial = serial,
                filter.StartDate,
                filter.EndDate
            };

            using (var connection = connector.GetConnection())
            {
                var timeout = connector.DefaultCommandTimeout;
                var eventsDataDto = await connection.QueryAsync<AtlasEventDto>(
                    new CommandDefinition(eventsDataSql, inputs, cancellationToken: ct, commandTimeout: timeout)).ConfigureAwait(false);

                return DtoConverters.CreateEventsFromDto(eventsDataDto);
            }
        }
    }
}
