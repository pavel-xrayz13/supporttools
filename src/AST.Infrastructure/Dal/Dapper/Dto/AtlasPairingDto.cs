﻿using System;

namespace AST.Infrastructure.Dal.Dapper.Dto
{
    public class AtlasPairingDto
    {
        public int DeviceId { get; set; }
        public int UserId { get; set; }
        public DateTime PairCreated { get; set; }
        public string UserLogin { get; set; }
    }
}
