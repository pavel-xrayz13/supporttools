﻿using AST.Infrastructure.Domain.Entities;
using System.Collections.Generic;
using System.Linq;

namespace AST.Infrastructure.Dal.Dapper.Dto
{
    public static class DtoConverters
    {
        public static AtlasStatusEntity CreateEntityFromDto(AtlasStatusDto dto, 
            AtlasPairingDto pairingDto, IEnumerable<AtlasHistoryDto> historyDto)
        {
            if (dto == null)
                return AtlasStatusEntity.Default();

            var result = new AtlasStatusEntity(dto.Id, dto.DeviceId, 
                DtoEnumConverters.ConvertDeviceStatusFromString(dto.DeviceStatus),
                DtoEnumConverters.ConvertAuthEquipFromString(dto.AuthEquip),
                DtoEnumConverters.ConvertAuthImmoPin(dto.AuthImmoPin),
                DtoEnumConverters.ConvertAuthImmoTag(dto.AuthImmoTag),
                DtoEnumConverters.ConvertAuthSlave(dto.AuthSlave));

            result.SetSimCard(CreateSimCardFromDto(dto));

            if (pairingDto != null)
                result.SetPairing(CreatePairingFromDto(pairingDto));

            if (historyDto.Any())
                result.AddHistory(CreateHistoryFromDto(historyDto));

            return result;
        }

        public static AtlasGeoDataEntity CreateGeoDataFromDto(AtlasGeoDataDto dto)
        {
            if (dto == null)
                return AtlasGeoDataEntity.Default();

            return new AtlasGeoDataEntity(dto.GeoDataLattitude, dto.GeoDataLongitude, dto.GeoDataCreated);
        }

        public static IEnumerable<AtlasEventEntity> CreateEventsFromDto(IEnumerable<AtlasEventDto> dtos)
        {
            var result = new List<AtlasEventEntity>();
            if (!dtos.Any())
                return result;

            foreach (var dto in dtos.ToList())
                result.Add(CreateEventEntityFromDto(dto));

            return result;
        }

        private static AtlasSimCardEntity CreateSimCardFromDto(AtlasStatusDto dto) =>
            new AtlasSimCardEntity(dto.SimCardType, dto.Iccid, dto.SimCardLastBalanceValue, dto.SimCardLastBalanceCheck);

        private static IEnumerable<AtlasHistoryEntity> CreateHistoryFromDto(IEnumerable<AtlasHistoryDto> list) =>
            list.Select(e => new AtlasHistoryEntity(DtoEnumConverters.ConvertDeviceStatusFromString(e.DeviceStatus), e.Created)).ToList();

        private static AtlasPairingEntity CreatePairingFromDto(AtlasPairingDto dto)
        {
            if (dto == null)
                return AtlasPairingEntity.Default();

            return new AtlasPairingEntity(dto.UserId, dto.DeviceId, dto.PairCreated, dto.UserLogin);
        }

        private static AtlasEventEntity CreateEventEntityFromDto(AtlasEventDto dto)
        {
            var eventDef = (AtlasEventsDefinition)dto.EventCode;
            var isAlarming = dto.IsAlarm == "yes";
            if(!isAlarming)
                isAlarming = eventDef.IsAlarming(dto.EventData);

            return new AtlasEventEntity(dto.Created, eventDef, dto.EventData, isAlarming);
        }
    }
}
