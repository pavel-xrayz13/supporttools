﻿using System;

namespace AST.Infrastructure.Dal.Dapper.Dto
{
    public class AtlasGeoDataDto
    {
        public int GeoDataLattitude { get; set; }
        public int GeoDataLongitude { get; set; }
        public DateTime GeoDataCreated { get; set; }
    }
}
