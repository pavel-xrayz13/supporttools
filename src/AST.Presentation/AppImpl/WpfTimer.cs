﻿using AST.Infrastructure.Bussiness;
using System;
using System.Windows.Threading;

namespace AST.Presentation.AppImpl
{
    public class WpfTimer : ITimer
    {
        private readonly DispatcherTimer timer;

        public WpfTimer(Action action, int timeout)
        {
            timer = new DispatcherTimer()
            {
                Interval = new TimeSpan(0, 0, timeout)
            };

            timer.Tick += (s, e) => action();
        }

        public void Start()
        {
            timer.Start();
        }

        public void Stop()
        {
            timer.Stop();
        }
    }
}
