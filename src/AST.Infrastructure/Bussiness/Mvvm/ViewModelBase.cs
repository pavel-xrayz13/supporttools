﻿using System.ComponentModel;

namespace AST.Infrastructure.Bussiness.Mvvm
{
    public abstract class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void FirePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, PropertyChangedEventArgsCache.Instance.Get(propertyName));
        }

        protected bool IsValidPropertyChangedEventHandler => PropertyChanged != null;
    }
}
