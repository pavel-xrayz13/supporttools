﻿using AST.Infrastructure.Domain.Entities;
using System;
using System.Collections.Specialized;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace AST.Presentation.Converters
{
    public class AtlasEventsDescriptionToStringConverter : IValueConverter
    {
        private static readonly StringDictionary eventsCacheDictionary = new StringDictionary();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var eventObject = (AtlasEventEntity)value;
            switch (eventObject.Definition)
            {
                case AtlasEventsDefinition.ResetToFactory:
                    return BuildResetToFactory(eventObject.Definition, eventObject.Data);

                case AtlasEventsDefinition.RoamingEvent:
                    return BuildRoamingEvent(eventObject.Definition, eventObject.Data);

                case AtlasEventsDefinition.FirmwareUpdate:
                    return BuildFirmwareUpdate(eventObject.Definition, eventObject.Data);

                case AtlasEventsDefinition.LogLoading:
                    return BuildLogLoading(eventObject.Definition, eventObject.Data);

                case AtlasEventsDefinition.LogCleanup:
                    return BuildLogCleanup(eventObject.Definition, eventObject.Data);

                case AtlasEventsDefinition.UssdRequest:
                    return BuildUssdRequest(eventObject.Definition, eventObject.Data);

                case AtlasEventsDefinition.IgnitionInputChange:
                    return BuildIgnitionInputChange(eventObject.Definition, eventObject.Data);

                case AtlasEventsDefinition.AlarmInputChange:
                    return BuildAlarmInputChange(eventObject.Definition, eventObject.Data);

                case AtlasEventsDefinition.AutostartStatusInputChange:
                    return BuildAutostartStatusInputChange(eventObject.Definition, eventObject.Data);

                case AtlasEventsDefinition.UniversalEventStatusChange:
                    return BuildUniversalEventStatusChange(eventObject.Definition, eventObject.Data);

                case AtlasEventsDefinition.AutostartEvent:
                    return BuildAutostartEvent(eventObject.Definition, eventObject.Data);

                case AtlasEventsDefinition.TemperatureStatus:
                    return BuildTemperatureStatus(eventObject.Definition, eventObject.Data);

                case AtlasEventsDefinition.BatteryVoltageStatus:
                    return BuildBatteryVoltageStatus(eventObject.Definition, eventObject.Data);

                case AtlasEventsDefinition.ImmoChangeStatus:
                    return BuildSecurityImmoChangeStatus(eventObject.Definition, eventObject.Data);

                case AtlasEventsDefinition.SecurityChangeToEnabled:
                    return BuildSecurityChangeToEnabled(eventObject.Definition, eventObject.Data);

                case AtlasEventsDefinition.SecurityChangeToDisabled:
                    return BuildSecurityChangeToDisabled(eventObject.Definition, eventObject.Data);

                case AtlasEventsDefinition.Alert:
                    return BuildAlarm(eventObject.Definition, eventObject.Data);

                case AtlasEventsDefinition.CentralLocking:
                    return BuildCenteralLocking(eventObject.Definition, eventObject.Data);

                case AtlasEventsDefinition.SmartphoneTag:
                    return BuildSmartphoneTag(eventObject.Definition, eventObject.Data);

                case AtlasEventsDefinition.ServiceMode:
                    return BuildServiceMode(eventObject.Definition, eventObject.Data);

                case AtlasEventsDefinition.AuthStatus:
                    return BuildAuthStatus(eventObject.Definition, eventObject.Data);

                case AtlasEventsDefinition.LowBatteryInTag:
                    return BuildLowBatteryInTag(eventObject.Definition, eventObject.Data);

                case AtlasEventsDefinition.SystemReset:
                case AtlasEventsDefinition.JammingDetection:
                case AtlasEventsDefinition.TimeSync:
                case AtlasEventsDefinition.AntihijackDisable:
                case AtlasEventsDefinition.NewKeyFlashing:
                case AtlasEventsDefinition.TagRewrite:
                case AtlasEventsDefinition.PinChangeMode:
                    return RequestStringForSimpleAtlasEvent(eventObject.Definition);

                case AtlasEventsDefinition.Ignition:
                case AtlasEventsDefinition.Engine:
                case AtlasEventsDefinition.DriverDoor:
                case AtlasEventsDefinition.PassengerDoor:
                case AtlasEventsDefinition.PassengerDoors:
                case AtlasEventsDefinition.RearLeftDoor:
                case AtlasEventsDefinition.ReadRightDoor:
                case AtlasEventsDefinition.Trunk:
                case AtlasEventsDefinition.Bonnet:
                    return BuildEventDefitionForTrigger(eventObject.Definition, eventObject.Data);

                default:
                    return BuildUnknownEvent(eventObject.Definition);
            }
        }

        public virtual object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        private string BuildEventDefitionForTrigger(AtlasEventsDefinition def, byte[] data)
        {
            var triggerState = def.AsAtlasTriggerStatus(data);
            return RequestStringFor(def, triggerState);
        }

        private string BuildResetToFactory(AtlasEventsDefinition def, byte[] data)
        {
            var state = def.AsAtlasEventResetToFactorySettings(data);
            return RequestStringFor(def, state);
        }

        private string BuildRoamingEvent(AtlasEventsDefinition def, byte[] data)
        {
            var state = def.AsAtlasEventRoamingEvent(data);
            return RequestStringFor(def, state);
        }

        private string BuildFirmwareUpdate(AtlasEventsDefinition def, byte[] data)
        {
            var state = def.AsAtlasEventFirmwareUpdateEvent(data);
            return RequestStringFor(def, state);
        }

        private string BuildLogLoading(AtlasEventsDefinition def, byte[] data)
        {
            var state = def.AsAtlasEventLogLoadingEvent(data);
            return RequestStringFor(def, state);
        }

        private string BuildLogCleanup(AtlasEventsDefinition def, byte[] data)
        {
            var state = def.AsAtlasLogCleanup(data);
            return RequestStringFor(def, state.Item1);
        }

        private string BuildUssdRequest(AtlasEventsDefinition def, byte[] data)
        {
            var state = def.AsAtlasUssdRequest(data);
            return RequestStringFor(def, state);
        }

        private string BuildIgnitionInputChange(AtlasEventsDefinition def, byte[] data)
        {
            var state = def.AsAtlasIgnitionInputChange(data);
            return RequestStringFor(def, state);
        }

        private string BuildAutostartStatusInputChange(AtlasEventsDefinition def, byte[] data)
        {
            var state = def.AsAtlasAutostartStatusInputChange(data);
            return RequestStringFor(def, state);
        }

        private string BuildAlarmInputChange(AtlasEventsDefinition def, byte[] data)
        {
            var state = def.AsAtlasAlarmInputChange(data);
            return RequestStringFor(def, state);
        }

        private string BuildUniversalEventStatusChange(AtlasEventsDefinition def, byte[] data)
        {
            var state = def.AsAtlasUniversalEventStatusChangeRaw(data);
            return RequestStringForAtlasEventWithFormat(def, state.Item1.ToString("X2"), state.Item2.ToString("X2"));
        }

        private string BuildAutostartEvent(AtlasEventsDefinition def, byte[] data)
        {
            var state = def.AsAtlasEventAutostart(data);
            if (state == AtlasEventAutostart.AutostartActivateByBlock)
            {
                return RequestStringForAtlasEventWithFormat(def,
                    state.AsAtlasEventAutoStartByteAtIndex(data, AtlasEventAutostartActivateByBlockData.StartDuration));
            }

            return RequestStringFor(def, state);
        }

        private string BuildBatteryVoltageStatus(AtlasEventsDefinition def, byte[] data)
        {
            var state = def.AsAtlasEventBatteryVoltageStatus(data);
            return RequestStringForAtlasEventWithFormat(def, state.ToString());
        }

        private string BuildTemperatureStatus(AtlasEventsDefinition def, byte[] data)
        {
            var state = def.AsAtlasEventTemperatureStatus(data);
            return RequestStringForAtlasEventWithFormat(def, state.ToString());
        }

        private string BuildSecurityImmoChangeStatus(AtlasEventsDefinition def, byte[] data)
        {
            var state = def.AsAtlasImmoChangeStatus(data);
            return RequestStringFor(def, state);
        }

        private string BuildSecurityChangeToEnabled(AtlasEventsDefinition def, byte[] data)
        {
            var state = def.AsAtlasSecurityChangeToEnabled(data);
            return RequestStringFor(def, state);
        }

        private string BuildSecurityChangeToDisabled(AtlasEventsDefinition def, byte[] data)
        {
            var state = def.AsAtlasSecurityChangeToDisabled(data);
            return RequestStringFor(def, state);
        }

        private string BuildAlarm(AtlasEventsDefinition def, byte[] data)
        {
            var state = def.AsAtlasEventAlert(data);
            return RequestStringFor(def, state);
        }

        private string BuildCenteralLocking(AtlasEventsDefinition def, byte[] data)
        {
            var state = def.AsAtlasEventCentralLocking(data);
            return RequestStringFor(def, state);
        }

        private string BuildSmartphoneTag(AtlasEventsDefinition def, byte[] data)
        {
            var state = def.AsAtlasEventSmartphoneTag(data);
            return RequestStringFor(def, state);
        }

        private string BuildServiceMode(AtlasEventsDefinition def, byte[] data)
        {
            var state = def.AsAtlasServiceMode(data);
            return RequestStringFor(def, state);
        }

        private string BuildAuthStatus(AtlasEventsDefinition def, byte[] data)
        {
            var state = def.AsAtlasAuthStatus(data);
            return RequestStringFor(def, state, tryWithResourceOnFailure: "Default");
        }

        private string BuildLowBatteryInTag(AtlasEventsDefinition def, byte[] data)
        {
            var state = def.AsAtlasLowBatteryTagStatus(data);
            return RequestStringForAtlasEvent(def, state.ToString());
        }

        private string BuildUnknownEvent(AtlasEventsDefinition def)
        {
            return RequestStringFromResources($"{nameof(AtlasEventsDefinition)}UnknownEvent");
        }

        private string RequestStringForAtlasEventWithFormat(AtlasEventsDefinition def, params string[] formatData)
        {
            var atlasEventStringToFormat = RequestStringForAtlasEvent(def, customString: string.Empty, isAlwaysUnique: true);
            if (string.IsNullOrWhiteSpace(atlasEventStringToFormat))
                return string.Empty;

            return string.Format(atlasEventStringToFormat, args: formatData);
        }

        private string RequestStringForAtlasEventWithFormat<TRequest>(AtlasEventsDefinition def, TRequest request, params string[] formatData)
        {
            var atlasEventStringToFormat = RequestStringFor(def, request, isAlwaysUnique: true);
            if (string.IsNullOrWhiteSpace(atlasEventStringToFormat))
                return string.Empty;

            return string.Format(atlasEventStringToFormat, args: formatData);
        }

        private string RequestStringForSimpleAtlasEvent(AtlasEventsDefinition def) =>
            RequestStringForAtlasEvent(def);

        private string RequestStringFor<TRequest>(AtlasEventsDefinition def, TRequest request,
            bool isAlwaysUnique = false, string tryWithResourceOnFailure = "")
        {
            return RequestStringForAtlasEvent(def, request.ToString(), isAlwaysUnique, tryWithResourceOnFailure);
        }

        private string RequestStringForAtlasEvent(AtlasEventsDefinition def, string customString = "", 
            bool isAlwaysUnique = false, string tryWithResourceOnFailure = "")
        {
            var requestingString = $"{nameof(AtlasEventsDefinition)}{def.ToString()}{customString}";
            var failbackResource = string.Empty;
            if(!string.IsNullOrWhiteSpace(tryWithResourceOnFailure))
                failbackResource = $"{nameof(AtlasEventsDefinition)}{def.ToString()}{tryWithResourceOnFailure}";

            return RequestStringFromResources(requestingString, tryWithResourceOnFailure: failbackResource);
        }

        private string RequestStringFromResources(string str, bool isAlwaysUnique = false, string tryWithResourceOnFailure = "")
        {
            if (!isAlwaysUnique)
            {
                if (eventsCacheDictionary.ContainsKey(str))
                    return eventsCacheDictionary[str];
            }

            var result = Application.Current.TryFindResource(str);
            if (result == null)
            {
                if (!string.IsNullOrWhiteSpace(tryWithResourceOnFailure))
                    result = Application.Current.TryFindResource(tryWithResourceOnFailure);
                else
                    result = string.Empty;
            }

            if (!isAlwaysUnique)
                eventsCacheDictionary.Add(str, (string)result);

            return (string)result;
        }
    }
}
