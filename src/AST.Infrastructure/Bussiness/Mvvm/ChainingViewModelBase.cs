﻿using System.Collections.Generic;

namespace AST.Infrastructure.Bussiness.Mvvm
{
    public abstract class ChainingViewModelBase : ValidatingViewModelBase
    {
        protected class PropertyChain
        {
            private readonly List<string> chain;

            public PropertyChain(List<string> chain)
            {
                this.chain = chain;
            }

            public PropertyChain ThenFireProperty(string name)
            {
                chain.Add(name);
                return this;
            }
        }

        private readonly Dictionary<string, List<string>> propertiesChaining;

        protected ChainingViewModelBase()
        {
            propertiesChaining = new Dictionary<string, List<string>>();
        }

        protected PropertyChain WhenFireProperty(string name)
        {
            PropertyChain propertyChain = null;
            if (!propertiesChaining.ContainsKey(name))
                propertiesChaining[name] = new List<string>();

            propertyChain = new PropertyChain(propertiesChaining[name]);
            return propertyChain;
        }

        protected override void FirePropertyChanged(string propertyName)
        {
            base.FirePropertyChanged(propertyName);
            if(propertiesChaining.ContainsKey(propertyName))
            {
                foreach(var dependencyProperty in propertiesChaining[propertyName])
                {
                    base.FirePropertyChanged(dependencyProperty);
                }
            }
        }
    }
}
