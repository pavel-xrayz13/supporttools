﻿using AST.Infrastructure.Bussiness.ViewModels;
using AST.Infrastructure.Domain.Entities;
using Autofac;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace AST.Infrastructure.Bussiness.Mediator
{
    public class AtlasGeoDataReadyNotify : INotification
    {
        public AtlasGeoDataEntity Entity { get; }

        public AtlasGeoDataReadyNotify(AtlasGeoDataEntity entity)
        {
            Entity = entity ?? throw new ArgumentNullException(nameof(entity));
        }
    }

    public class AtlasGeoDataReadyMediatorNotifier : INotificationHandler<AtlasGeoDataReadyNotify>
    {
        private readonly IComponentContext context;

        public AtlasGeoDataReadyMediatorNotifier(IComponentContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public Task Handle(AtlasGeoDataReadyNotify notification, CancellationToken ct)
        {
            if (!ct.IsCancellationRequested)
            {
                var destinationViewModel = context.Resolve<AtlasGeoDataViewModel>();
                destinationViewModel.Entity = notification.Entity;
            }
            return Task.CompletedTask;
        }
    }
}
