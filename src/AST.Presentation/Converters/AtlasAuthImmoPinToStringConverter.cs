﻿using AST.Infrastructure.Domain.Entities;

namespace AST.Presentation.Converters
{
    internal class AtlasAuthImmoPinToStringConverter : GenericOneWayEnumConverter<AtlasAuthImmoPin>
    {
    }
}
