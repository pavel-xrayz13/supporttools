﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace AST.Presentation.Converters
{
    internal abstract class GenericOneWayEnumConverter<TEnum> : IValueConverter
        where TEnum : Enum
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((TEnum)value).ToString();
        }

        public virtual object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
