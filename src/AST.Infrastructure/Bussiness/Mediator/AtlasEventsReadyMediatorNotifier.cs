﻿using AST.Infrastructure.Domain.Entities;
using AST.Infrastructure.Bussiness.ViewModels;
using Autofac;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace AST.Infrastructure.Bussiness.Mediator
{
    public class AtlasEventsReadyNotify : INotification
    {
        public IEnumerable<AtlasEventEntity> Entities { get; }

        public AtlasEventsReadyNotify(IEnumerable<AtlasEventEntity> entities)
        {
            Entities = entities ?? throw new ArgumentNullException(nameof(entities));
        }
    }

    public class AtlasEventsReadyMediatorNotifier : INotificationHandler<AtlasEventsReadyNotify>
    {
        private readonly IComponentContext componentContext;

        public AtlasEventsReadyMediatorNotifier(IComponentContext componentContext)
        {
            this.componentContext = componentContext ?? throw new ArgumentNullException(nameof(componentContext));
        }

        public Task Handle(AtlasEventsReadyNotify notification, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var workspace = componentContext.Resolve<AtlasEventsDataViewModel>();
            workspace.Entity = notification.Entities;
            return Task.CompletedTask;
        }
    }
}
