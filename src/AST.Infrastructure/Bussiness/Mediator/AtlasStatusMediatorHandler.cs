﻿using AST.Infrastructure.Dal;
using AST.Infrastructure.Domain.Entities;
using AST.Infrastructure.Domain.QueryModel;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace AST.Infrastructure.Bussiness.Mediator
{
    public class AtlasStatusMediatorHandler : IRequestHandler<SerialNumberForStatusQueryModel, AtlasStatusEntity>
    {
        private readonly IAtlasStatusRepository atlasStatusRepository;

        public AtlasStatusMediatorHandler(IAtlasStatusRepository atlasStatusRepository)
        {
            this.atlasStatusRepository = atlasStatusRepository ?? throw new ArgumentNullException(nameof(atlasStatusRepository));
        }

        public Task<AtlasStatusEntity> Handle(SerialNumberForStatusQueryModel request, CancellationToken cancellationToken) =>
            atlasStatusRepository.GetAtlasStatusBySerialId(request.Serial, cancellationToken);
    }
}
