﻿using AST.Infrastructure.Domain.Entities;
using System;

namespace AST.Infrastructure.Dal.Dapper.Dto
{
    public static class DtoEnumConverters
    {
        public static AtlasDeviceStatus ConvertDeviceStatusFromString(string deviceStatus)
        {
            var status = deviceStatus.ToLower();
            if (status == "offline")
                return AtlasDeviceStatus.Offline;

            if (status == "online")
                return AtlasDeviceStatus.Online;

            if (status == "never")
                return AtlasDeviceStatus.Never;

            throw new InvalidCastException($"Failed to cast DeviceStatus string to {typeof(AtlasDeviceStatus)} enumeable");
        }

        public static AtlasAuthEquip ConvertAuthEquipFromString(string authEquip)
        {
            var status = authEquip.ToLower();
            if (status == "pin_only")
                return AtlasAuthEquip.PinOnly;

            if (status == "extended")
                return AtlasAuthEquip.Extended;

            throw new InvalidCastException($"Failed to cast DeviceStatus string to {typeof(AtlasAuthEquip)} enumeable");
        }

        public static AtlasAuthProtecting ConvertAuthProcessingFromString(string authProcessing)
        {
            var status = authProcessing.ToLower();
            if (status == "off")
                return AtlasAuthProtecting.Off;

            if (status == "on")
                return AtlasAuthProtecting.On;

            throw new InvalidCastException($"Failed to cast DeviceStatus string to {typeof(AtlasAuthProtecting)} enumeable");

        }

        public static AtlasAuthImmoPin ConvertAuthImmoPin(string authImmoPin)
        {
            var status = authImmoPin.ToLower();
            if (status == "authorized")
                return AtlasAuthImmoPin.Authorized;

            if (status == "protecting")
                return AtlasAuthImmoPin.Protecting;

            throw new InvalidCastException($"Failed to cast AuthImmoPin string to {typeof(AtlasAuthImmoPin)} enumerable");
        }

        public static AtlasAuthImmoTag ConvertAuthImmoTag(string authImmoTag)
        {
            var status = authImmoTag.ToLower();
            if (status == "unknown")
                return AtlasAuthImmoTag.Unknown;

            if (status == "authorized")
                return AtlasAuthImmoTag.Authorized;

            if (status == "protecting")
                return AtlasAuthImmoTag.Protecting;

            throw new InvalidCastException($"Failed to cast AuthImmoTag string to {typeof(AtlasAuthImmoPin)} enumerable");
        }

        public static AtlasAuthSlave ConvertAuthSlave(string authSlave)
        {
            var status = authSlave.ToLower();
            if (status == "unknown")
                return AtlasAuthSlave.Unknown;

            if (status == "authorized")
                return AtlasAuthSlave.Authorized;

            if (status == "protecting")
                return AtlasAuthSlave.Protecting;

            throw new InvalidCastException($"Failed to cast AuthSlave string to {typeof(AtlasAuthImmoPin)} enumerable");
        }

    }
}
