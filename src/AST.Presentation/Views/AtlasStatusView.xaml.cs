﻿using AST.Infrastructure.Bussiness;
using System.Windows.Controls;
using Autofac;
using MahApps.Metro.Controls;

namespace AST.Presentation.Views
{
    /// <summary>
    /// Interaction logic for AtlasStatusView.xaml
    /// </summary>
    public partial class AtlasStatusView : UserControl
    {
        public AtlasStatusView()
        {
            InitializeComponent();

            var applicationWorkspace = IocKernel.Default.Container.Resolve<IApplicationWorkspace>();
            var window = IocKernel.Default.Container.Resolve<MetroWindow>();
            applicationWorkspace.OnCancelAsync += (s, e) =>
            {
                window.Invoke(() =>
                {
                    AtlasStatusTabControl.SelectedIndex = 0;
                });
            };
        }
    }
}
