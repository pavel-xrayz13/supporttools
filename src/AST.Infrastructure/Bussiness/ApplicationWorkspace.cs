﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AST.Infrastructure.Bussiness
{
    public class TracingWorkForApplication : ITracingWorkForApplication
    {
        private readonly CancellationTokenSource cts;

        public event EventHandler<EventArgs> OnDispose;

        public TracingWorkForApplication(CancellationTokenSource cts)
        {
            this.cts = cts;
        }

        public CancellationToken Token => cts.Token;

        public void Dispose() => OnDispose?.Invoke(this, EventArgs.Empty);
    }

    public class ApplicationWorkspace : IApplicationWorkspace
    {
        private readonly CancellationTokenSource cts = new CancellationTokenSource();
        private readonly ICollection<TracingWorkForApplication> availableWork = new List<TracingWorkForApplication>();

        private readonly IMessageBoxProvider messageBoxProvider;

        public event EventHandler<EventArgs> OnCancelAsync;

        public ApplicationWorkspace(IMessageBoxProvider messageBoxProvider)
        {
            this.messageBoxProvider = messageBoxProvider;
        }

        public bool ProcessOperation => availableWork.Any();

        public async Task CancelAllAsync()
        {
            if (await messageBoxProvider.ShowConfirmationAsync("", "Отменить текущие выполняющеся задачи?"))
            {
                OnCancelAsync?.Invoke(this, EventArgs.Empty);
                cts.Cancel();
            }
        }

        public ITracingWorkForApplication CreateTracingWork()
        {
            var workForApplication = new TracingWorkForApplication(cts);
            workForApplication.OnDispose += OnDisposeEvent;

            availableWork.Add(workForApplication);
            return workForApplication;
        }

        private void OnDisposeEvent(object sender, EventArgs args)
        {
            var self = (TracingWorkForApplication)sender;
            availableWork.Remove(self);
        }
    }
}
