﻿using AST.Infrastructure.Bussiness;
using System;

namespace AST.Presentation.AppImpl
{
    class WpfTimerFactory : ITimerFactory
    {
        public ITimer CreateTimer(Action action, int timeout) =>
            new WpfTimer(action, timeout);
    }
}
