﻿using System;
using System.Collections.Generic;

namespace AST.Infrastructure.Domain.Entities
{
    public class AtlasStatusEntity : IEquatable<AtlasStatusEntity>
    {
        private readonly List<AtlasHistoryEntity> historyEntries;

        public int Id { get; }
        public string DeviceId { get; }
        public AtlasDeviceStatus DeviceStatus { get; private set; }
        public AtlasSimCardEntity SimCard { get; private set; }
        public AtlasPairingEntity Pairing { get; private set; }
        public AtlasAuthEquip AuthEquip { get; private set; }
        public AtlasAuthImmoPin AuthImmoPin { get; private set; }
        public AtlasAuthImmoTag AuthImmoTag { get; private set; }
        public AtlasAuthSlave AuthSlave { get; private set; }
        public IEnumerable<AtlasHistoryEntity> History => historyEntries;

        public AtlasStatusEntity(int id, string deviceId,
            AtlasDeviceStatus status, AtlasAuthEquip auth,
            AtlasAuthImmoPin immoPin, AtlasAuthImmoTag immoTag, AtlasAuthSlave authSlave)
        {
            Id = id;
            DeviceId = deviceId;
            DeviceStatus = status;

            SimCard = AtlasSimCardEntity.Default();
            Pairing = AtlasPairingEntity.Default();

            AuthEquip = auth;
            AuthImmoPin = immoPin;
            AuthImmoTag = immoTag;
            AuthSlave = authSlave;

            historyEntries = new List<AtlasHistoryEntity>();
        }

        public static AtlasStatusEntity Default() => 
            new AtlasStatusEntity(default, string.Empty, 
                AtlasDeviceStatus.Never, AtlasAuthEquip.PinOnly,
                AtlasAuthImmoPin.Authorized, AtlasAuthImmoTag.Unknown, AtlasAuthSlave.Unknown);

        public void SetSimCard(AtlasSimCardEntity card)
        {
            SimCard = card ?? throw new ArgumentNullException(nameof(card));
        }

        public void SetPairing(AtlasPairingEntity pairing)
        {
            Pairing = pairing ?? throw new ArgumentNullException(nameof(pairing));
        }

        public void AddHistory(IEnumerable<AtlasHistoryEntity> list) => historyEntries.AddRange(list);

        public bool IsDefault()
        {
            return Id == default && DeviceId == string.Empty;
        }

        public bool Equals(AtlasStatusEntity other)
        {
            return this.Equals((object)other);
        }
    }
}
