﻿using AST.Infrastructure;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Threading.Tasks;

namespace AST.Presentation.AppImpl
{
    public class MahAppsMessageBoxProvider : IMessageBoxProvider, IErrorHandler
    {
        private readonly MetroWindow window;

        public MahAppsMessageBoxProvider(MetroWindow window)
        {
            this.window = window ?? throw new ArgumentNullException(nameof(window));
        }

        public async Task HandleErrorAsync(Exception ex)
        {
            await window.Dispatcher.InvokeAsync(async () =>
            {
                await window.ShowMessageAsync("Внутренняя ошибка приложения", ex.Message);
            });
        }

        public async Task<bool> ShowConfirmationAsync(string header, string message)
        {
            bool result = false;
            await window.Dispatcher.InvokeAsync(async () =>
            {
                var dialogResult = await window.ShowMessageAsync(header, message, MessageDialogStyle.AffirmativeAndNegative);
                result = dialogResult == MessageDialogResult.Affirmative;
            });
            return result;
        }

        public async Task ShowMessageAsync(string header, string message)
        {
            await window.Dispatcher.InvokeAsync(async () =>
            {
                await window.ShowMessageAsync(header, message);
            });
        }
    }
}
