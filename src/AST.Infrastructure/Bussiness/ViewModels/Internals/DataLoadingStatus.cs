﻿namespace AST.Infrastructure.Bussiness.ViewModels.Internals
{
    public enum DataLoadingStatus
    {
        NeverLoadedWithThisEntity,
        LoadingNow,
        CanRefresh
    }
}
