﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace AST.Infrastructure
{
    public static class Extensions
    {
        public static void FireAndForgetSafeAsync(Func<Task> task, IErrorHandler handler, bool continueOnCapturedContext = false)
        {
            if (task == null)
                throw new NullReferenceException();

            if (handler == null)
                throw new ArgumentNullException(nameof(handler));

            Task.Run(async () =>
            {
                try
                {
                    await task().ConfigureAwait(continueOnCapturedContext);
                }
                catch (Exception ex)
                {
                    await handler.HandleErrorAsync(ex);
                }
            });
        }

        public static DateTime DateTimeToBeginDay(this DateTime self)
        {
            return new DateTime(self.Year, self.Month, self.Day, 0, 0, 0);
        }

        public static DateTime DateTimeToEndDay(this DateTime self)
        {
            return new DateTime(self.Year, self.Month, self.Day, 23, 59, 59);
        }
    }

    public static class EnumConverter<TEnum> where TEnum : struct
    {
        public static readonly Func<long, TEnum> Convert = GenerateConverter();

        static Func<long, TEnum> GenerateConverter()
        {
            var parameter = Expression.Parameter(typeof(long));
            var dynamicMethod = Expression.Lambda<Func<long, TEnum>>(
                Expression.Convert(parameter, typeof(TEnum)), parameter);
            return dynamicMethod.Compile();
        }
    }
}
