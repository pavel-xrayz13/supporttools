﻿namespace AST.Infrastructure.Domain.Entities
{
    public enum AtlasAuthImmoPin
    {
        Authorized,
        Protecting
    }
}
