﻿using System;

namespace AST.Infrastructure.Domain.Entities
{
    public class AtlasPairingEntity
    {
        private readonly string phone;

        public int Device { get; }
        public int User { get; }
        public DateTime Created { get; }
        public bool HasPair => User != default;
        public bool NoPair => !HasPair;
        public string GetPhoneLastFourSymbols => GetLastNumbersFromPhone(4);

        public AtlasPairingEntity(int userId, int deviceId, DateTime created, string phone)
        {
            if(userId > 0)
            {
                if (string.IsNullOrWhiteSpace(phone))
                    throw new ArgumentException("Pairing phone cannot be null, empty or whitespace");
            }

            User = userId;
            Device = deviceId;
            Created = created;
            this.phone = phone;
        }

        public static AtlasPairingEntity Default() => new AtlasPairingEntity(default, default, default, string.Empty);

        private string GetLastNumbersFromPhone(int count)
        {
            if (string.IsNullOrWhiteSpace(phone))
                return string.Empty;

            if (count > phone.Length)
                throw new InvalidOperationException($"phone.Length={phone.Length}; count={count}: count > phone.Length");

            var phoneNumberArray = phone.ToCharArray();
            if (count > phoneNumberArray.Length)
                throw new InvalidOperationException($"phoneNumber.Length={phoneNumberArray.Length}; count={count}: count > phone.Length");

            var replacementCount = phoneNumberArray.Length - count;
            for (var currentIndex = 0; currentIndex < replacementCount; ++currentIndex)
                phoneNumberArray[currentIndex] = '*';

            return new string(phoneNumberArray);
        }
    }
}
