﻿using AST.Infrastructure.Dal;
using AST.Infrastructure.Domain.Entities;
using AST.Infrastructure.Domain.QueryModel;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace AST.Infrastructure.Bussiness.Mediator
{
    public class AtlasGeoDataMediatorHandler : IRequestHandler<SerialNumberForGeoDataQueryModel, AtlasGeoDataEntity>
    {
        private readonly IAtlasStatusRepository atlasStatusRepository;

        public AtlasGeoDataMediatorHandler(IAtlasStatusRepository atlasStatusRepository)
        {
            this.atlasStatusRepository = atlasStatusRepository ?? throw new ArgumentNullException(nameof(atlasStatusRepository));
        }

        public Task<AtlasGeoDataEntity> Handle(SerialNumberForGeoDataQueryModel request, CancellationToken cancellationToken) =>
            atlasStatusRepository.GetAtlasGeoDataBySerialId(request.Id, cancellationToken);
    }
}
