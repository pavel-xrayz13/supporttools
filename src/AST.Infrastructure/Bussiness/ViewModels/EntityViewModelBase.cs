﻿using AST.Infrastructure.Bussiness.Mvvm;

namespace AST.Infrastructure.Bussiness.ViewModels
{
    public abstract class EntityViewModelBase<TEntity> : ValidatingViewModelBase
        where TEntity : class
    {
        private TEntity entity;
        public TEntity Entity
        {
            get => entity;
            set
            {
                entity = value;
                FirePropertyChanged(nameof(Entity));
            }
        }
    }

    public abstract class ChainingEntityViewModelBase<TEntity> : ChainingViewModelBase
        where TEntity : class
    {
        private TEntity entity;
        public TEntity Entity
        {
            get => entity;
            set
            {
                entity = value;
                FirePropertyChanged(nameof(Entity));
            }
        }
    }
}
