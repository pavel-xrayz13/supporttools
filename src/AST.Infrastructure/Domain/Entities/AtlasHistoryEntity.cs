﻿using System;

namespace AST.Infrastructure.Domain.Entities
{
    public class AtlasHistoryEntity
    {
        public AtlasDeviceStatus DeviceStatus { get; }
        public DateTime Created { get; }

        public AtlasHistoryEntity(AtlasDeviceStatus status, DateTime time)
        {
            DeviceStatus = status;
            Created = time;
        }
    }
}
