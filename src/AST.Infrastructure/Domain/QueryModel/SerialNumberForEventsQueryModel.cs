﻿using AST.Infrastructure.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;

namespace AST.Infrastructure.Domain.QueryModel
{
    public class SerialNumberForEventsQueryModel : IRequest<IEnumerable<AtlasEventEntity>>
    {
        public string Serial { get; }
        public DateTime Date { get; }

        public SerialNumberForEventsQueryModel(string serial, DateTime dateTime)
        {
            Serial = serial;
            Date = dateTime;
        }
    }
}
