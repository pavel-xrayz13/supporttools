﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;

namespace AST.Infrastructure.Bussiness.Mvvm
{
    public class AsyncCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private bool isExecuting;
        private readonly Func<Task> execute;
        private readonly Func<bool> canExecute;
        private readonly IErrorHandler errorHandler;
        private readonly Dispatcher dispatcher;

        public AsyncCommand(IErrorHandler errorHandler, Dispatcher dispatcher,
            Func<Task> execute, Func<bool> canExecute = null)
        {
            this.execute = execute;
            this.canExecute = canExecute;
            this.errorHandler = errorHandler;
            this.dispatcher = dispatcher;
        }

        public bool CanExecute() => !isExecuting && (canExecute?.Invoke() ?? true);

        public async Task ExecuteAsync()
        {
            if (CanExecute())
            {
                try
                {
                    isExecuting = true;
                    await execute();
                }
                finally
                {
                    isExecuting = false;
                }
            }

            RaiseCanExecuteChanged();
        }

        public void RaiseCanExecuteChanged()
        {
            dispatcher.Invoke(() =>
            {
                CanExecuteChanged?.Invoke(this, EventArgs.Empty);
            });
        }

        #region ICommand implementations
        bool ICommand.CanExecute(object parameter)
        {
            return CanExecute();
        }

        void ICommand.Execute(object parameter)
        {
            Extensions.FireAndForgetSafeAsync(ExecuteAsync, errorHandler);
        }
        #endregion
    }
}
