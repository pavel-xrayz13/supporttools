﻿using System;

namespace AST.Infrastructure.Dal.Dapper.Dto
{
    public class AtlasHistoryDto
    {
        public string DeviceStatus { get; set; }
        public DateTime Created { get; set; }
    }
}
