﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace AST.Presentation.Converters
{
    internal class DateTimeToStringConverter : IValueConverter
    {
        private static readonly string DateFormat = "yyyy-MM-dd HH:mm:ss";

        public object Convert(object value, Type targetType, object defaultValueParameter, CultureInfo culture)
        {
            if (!(value is DateTime))
                throw new InvalidOperationException("Input type is not DateTime");

            var dateTime = (DateTime)value;
            if (dateTime == default)
                return string.Empty;

            return dateTime.ToString(DateFormat);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
