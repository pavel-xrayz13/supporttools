﻿using System;
using System.Diagnostics;
using System.ComponentModel;

namespace AST.Infrastructure.Bussiness.Mvvm
{
    public abstract class ValidatingViewModelBase : ViewModelBase
    {
        protected override void FirePropertyChanged(string propertyName)
        {
            VerifyPropertyName(propertyName);
            base.FirePropertyChanged(propertyName);
        }

        [Conditional("DEBUG")]
        [DebuggerStepThrough]
        protected virtual void VerifyPropertyName(string propertyName)
        {
            // Verify that the property name matches a real,  
            // public, instance property on this object.
            if (TypeDescriptor.GetProperties(this)[propertyName] == null)
                throw new Exception($"Invalid property name: {propertyName}");
        }
    }
}
