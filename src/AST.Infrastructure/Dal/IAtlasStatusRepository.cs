﻿using AST.Infrastructure.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace AST.Infrastructure.Dal
{
    public class GetAtlasEventsFilter
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }

    public interface IAtlasStatusRepository
    {
        Task<AtlasStatusEntity> GetAtlasStatusBySerialId(string serial, CancellationToken ct = default);

        Task<AtlasGeoDataEntity> GetAtlasGeoDataBySerialId(int id, CancellationToken ct = default);

        Task<IEnumerable<AtlasEventEntity>> GetAtlasEventsByFilter(string serial,
            GetAtlasEventsFilter filter, CancellationToken ct = default);
    }
}
