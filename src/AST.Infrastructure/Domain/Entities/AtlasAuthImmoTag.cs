﻿namespace AST.Infrastructure.Domain.Entities
{
    public enum AtlasAuthImmoTag
    {
        Unknown,
        Authorized,
        Protecting
    }
}
