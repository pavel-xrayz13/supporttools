using AST.Infrastructure.Bussiness.Mediator;
using AST.Infrastructure.Bussiness.Mvvm;
using AST.Infrastructure.Bussiness.ViewModels.Internals;
using AST.Infrastructure.Domain.QueryModel;
using Autofac;
using MediatR;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;

namespace AST.Infrastructure.Bussiness.ViewModels
{
    public class MainViewModel : SelectableWorkspaceViewModel
    {
        private readonly IComponentContext serviceLocator;
        private readonly IMediator mediator;
        private readonly IApplicationWorkspace applicationWorkspace;
        private readonly IErrorHandler errorHandler;
        private readonly Dispatcher dispatcher;
        private readonly ITimer notificationTimer;

        public MainViewModel(IComponentContext serviceLocator)
        {
            const int timeoutForNotification = 30; // seconds
            this.serviceLocator = serviceLocator ?? throw new ArgumentNullException(nameof(serviceLocator));

            errorHandler = serviceLocator.Resolve<IErrorHandler>();
            mediator = serviceLocator.Resolve<IMediator>();
            applicationWorkspace = serviceLocator.Resolve<IApplicationWorkspace>();
            dispatcher = serviceLocator.Resolve<Dispatcher>();

            notificationTimer = serviceLocator.Resolve<ITimerFactory>().CreateTimer(() =>
            {
                NotifyQueryTakesTooMuchTime = SerialNumberNotificationBadge.QueryTakesTooMuchTime;
            }, timeoutForNotification);

            QueryAtlasInformation = new AsyncCommand(errorHandler, dispatcher, 
                QueryAtlasInformationAsyncCommand);

            WhenFireProperty(nameof(DeviceProcessingNow)).ThenFireProperty(nameof(CanClickToFind));
            WhenFireProperty(nameof(CurrentSerialNumber)).ThenFireProperty(nameof(CanClickToFind));
            WhenFireProperty(nameof(ExternalOperation)).ThenFireProperty(nameof(CanClickToFind));

            SwitchToNoDataView();
        }

        private bool deviceProcessingNow = false;
        public bool DeviceProcessingNow
        {
            get => deviceProcessingNow;
            set
            {
                deviceProcessingNow = value;
                FirePropertyChanged(nameof(DeviceProcessingNow));
            }
        }

        private string currentSerialNumber;
        public string CurrentSerialNumber
        {
            get => currentSerialNumber;
            set
            {
                currentSerialNumber = value;
                FirePropertyChanged(nameof(CurrentSerialNumber));
            }
        }

        private bool externalOperation;
        public bool ExternalOperation
        {
            get => externalOperation;
            set
            {
                externalOperation = value;
                FirePropertyChanged(nameof(ExternalOperation));
            }
        }

        public bool CanClickToFind => !(string.IsNullOrWhiteSpace(CurrentSerialNumber)
                || DeviceProcessingNow || ExternalOperation);

        private SerialNumberNotificationBadge notifyQueryTakesTooMuchTime;
        public SerialNumberNotificationBadge NotifyQueryTakesTooMuchTime
        {
            get => notifyQueryTakesTooMuchTime;
            set
            {
                notifyQueryTakesTooMuchTime = value;
                FirePropertyChanged(nameof(NotifyQueryTakesTooMuchTime));
            }
        }

        public ICommand QueryAtlasInformation { get; }

        private void SwitchToNoDataView() =>
            CurrentViewModel = serviceLocator.Resolve<EmptyDataViewModel>();

        private void SwitchToLoadDataView() =>
            CurrentViewModel = serviceLocator.Resolve<DataLoadingViewModel>();

        private void SwitchToAtlasInformationView() => 
            CurrentViewModel = serviceLocator.Resolve<AtlasStatusViewModel>();

        private bool CanQueryAtlasInformationDoCommand() => 
            !(string.IsNullOrWhiteSpace(CurrentSerialNumber) || DeviceProcessingNow);

        private async Task QueryAtlasInformationAsyncCommand()
        {
            var messageBoxProvider = serviceLocator.Resolve<IMessageBoxProvider>();
            if (applicationWorkspace.ProcessOperation)
                await applicationWorkspace.CancelAllAsync();

            using (var unit = applicationWorkspace.CreateTracingWork())
            {
                try
                {
                    DeviceProcessingNow = true;
                    SwitchToLoadDataView();

                    NotifyQueryTakesTooMuchTime = SerialNumberNotificationBadge.QueryIsRunning;
                    FirePropertyChanged(nameof(CanClickToFind));

                    notificationTimer.Start();

                    await mediator.Publish(AtlasStatusResetNotify.Default, unit.Token);
                    var result = await mediator.Send(SerialNumberForStatusQueryModel.CreateQueryModel(CurrentSerialNumber), unit.Token);

                    if (result.IsDefault())
                    {
                        SwitchToNoDataView();
                        await messageBoxProvider.ShowMessageAsync("������", $"�� ������� ����� ���������� � �������� ������� {CurrentSerialNumber}.");
                    }
                    else
                    {
                        await mediator.Publish(new AtlasStatusReadyNotify(result), unit.Token);
                        SwitchToAtlasInformationView();
                    }
                }
                catch(TaskCanceledException)
                {
                    SwitchToNoDataView();
                }
                catch(Exception ex)
                {
                    SwitchToNoDataView();
                    await messageBoxProvider.ShowMessageAsync("������", $"���������� ������ ����������: {ex.Message}.");
                }

                notificationTimer.Stop();
                DeviceProcessingNow = false;
                NotifyQueryTakesTooMuchTime = SerialNumberNotificationBadge.NoBadge;
                FirePropertyChanged(nameof(CanClickToFind));
            }
        }
    }
}