﻿namespace AST.Infrastructure.Domain.Entities
{
    public enum AtlasAuthSlave
    {
        Unknown,
        Authorized,
        Protecting
    }
}
