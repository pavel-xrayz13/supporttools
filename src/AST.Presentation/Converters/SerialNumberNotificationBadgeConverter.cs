﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using AST.Infrastructure.Bussiness.ViewModels.Internals;

namespace AST.Presentation.Converters
{
    internal class SerialNumberNotificationBadgeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var enumeration = (SerialNumberNotificationBadge)value;
            switch(enumeration)
            {
                case SerialNumberNotificationBadge.NoBadge:
                    return string.Empty;
                case SerialNumberNotificationBadge.QueryIsRunning:
                    return string.Empty;
                case SerialNumberNotificationBadge.QueryTakesTooMuchTime:
                    return "!";
                default:
                    throw new InvalidOperationException();
            }
        }

        public virtual object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
