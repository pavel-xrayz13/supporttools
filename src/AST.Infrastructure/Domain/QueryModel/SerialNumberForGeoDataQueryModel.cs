﻿using AST.Infrastructure.Domain.Entities;
using MediatR;

namespace AST.Infrastructure.Domain.QueryModel
{
    public class SerialNumberForGeoDataQueryModel : IRequest<AtlasGeoDataEntity>
    {
        public int Id { get; }

        public SerialNumberForGeoDataQueryModel(int id)
        {
            Id = id;
        }

        public static SerialNumberForGeoDataQueryModel CreateQueryModel(int id) =>
            new SerialNumberForGeoDataQueryModel(id);
    }
}
