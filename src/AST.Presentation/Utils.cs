﻿using Autofac;
using System.Diagnostics;
using System.Reflection;

namespace AST.Presentation
{
    public class IocKernel
    {
        public static IocKernel Default { get; } = new IocKernel();
        public IContainer Container { get; set; }
    }

    public static class EnvironmentInfo
    {
        public static readonly string Version;
        public static readonly string FullVersion;

        static EnvironmentInfo()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var fileVersionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);

            var ver = fileVersionInfo.ProductVersion;

            var parts = ver.Split('.');
            var major = ushort.Parse(parts[0]);
            var minor = ushort.Parse(parts[1]);

            Version = $"{major}.{minor}";
            FullVersion = ver;
        }
    }
}
