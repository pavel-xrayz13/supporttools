﻿namespace AST.Infrastructure.Domain.Entities
{
    public enum AtlasDeviceStatus
    {
        Online,
        Offline,
        Never
    }
}
