﻿using AST.Infrastructure.Bussiness.GeoData;
using AST.Infrastructure.Bussiness.Mediator;
using AST.Infrastructure.Bussiness.Mvvm;
using AST.Infrastructure.Bussiness.ViewModels.Internals;
using AST.Infrastructure.Domain.Entities;
using AST.Infrastructure.Domain.QueryModel;
using MediatR;
using Autofac;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using System;
using System.Linq;

namespace AST.Infrastructure.Bussiness.ViewModels
{
    public class AtlasStatusViewModel : ChainingEntityViewModelBase<AtlasStatusEntity>
    {
        private readonly IGeoDataService geoDataService;
        private readonly IApplicationWorkspace applicationWorkspace;
        private readonly IMediator mediator;
        private readonly IErrorHandler errorHandler;
        private readonly Dispatcher dispatcher;
        private readonly IComponentContext componentContext;

        public AtlasStatusViewModel(IComponentContext componentContext)
        {
            this.componentContext = componentContext ?? throw new ArgumentNullException(nameof(componentContext));
            applicationWorkspace = componentContext.Resolve<IApplicationWorkspace>();
            geoDataService = componentContext.Resolve<IGeoDataService>();
            mediator = componentContext.Resolve<IMediator>();
            errorHandler = componentContext.Resolve<IErrorHandler>();
            dispatcher = componentContext.Resolve<Dispatcher>();

            ResetDataViewModel();

            applicationWorkspace.OnCancelAsync += (s, e) =>
            {
                ResetDataViewModel();
            };

            QueryGeoDataCommand = new AsyncCommand(errorHandler, dispatcher,
                QueryGeoDataInformationAsyncCommand, CanQueryGeoDataInformationDoCommand);
            QueryEventsDataCommand = new AsyncCommand(errorHandler, dispatcher,
                QueryEventsDataInformationAsyncCommand, CanQueryEventsDataInformationDoCommand);

            WhenFireProperty(nameof(GeoDataLoadingStatus))
                .ThenFireProperty(nameof(GeoDataProcessingNow))
                .ThenFireProperty(nameof(NoGeoDataProcessingNow));

            WhenFireProperty(nameof(GeoDataWorkspaceViewModel))
                .ThenFireProperty(nameof(GeoDataLoadingStatus));

            WhenFireProperty(nameof(EventsDataLoadingStatus))
                .ThenFireProperty(nameof(EventsDataProcessingNow))
                .ThenFireProperty(nameof(NoEventsDataProcessingNow));

            WhenFireProperty(nameof(EventsWorkspaceViewModel))
                .ThenFireProperty(nameof(EventsDataLoadingStatus));
        }

        public ICommand QueryGeoDataCommand { get; }

        public bool GeoDataProcessingNow => 
            GeoDataLoadingStatus == DataLoadingStatus.LoadingNow;

        public bool NoGeoDataProcessingNow => !GeoDataProcessingNow;

        private ViewModelBase geoDataWorkspaceViewModel;
        public ViewModelBase GeoDataWorkspaceViewModel
        {
            get => geoDataWorkspaceViewModel;
            set
            {
                geoDataWorkspaceViewModel = value;
                FirePropertyChanged(nameof(GeoDataWorkspaceViewModel));
            }
        }

        private DataLoadingStatus geoDataLoadingStatus;
        public DataLoadingStatus GeoDataLoadingStatus
        {
            get => geoDataLoadingStatus;
            set
            {
                geoDataLoadingStatus = value;
                FirePropertyChanged(nameof(GeoDataLoadingStatus));
            }
        }

        public ICommand QueryEventsDataCommand { get; }

        public bool EventsDataProcessingNow =>
            EventsDataLoadingStatus == DataLoadingStatus.LoadingNow;

        public bool NoEventsDataProcessingNow => !EventsDataProcessingNow;

        private DateTime filteringDateTime;
        public DateTime FilteringDateTime
        {
            get => filteringDateTime;
            set
            {
                filteringDateTime = value;
                FirePropertyChanged(nameof(FilteringDateTime));
            }
        }

        private ViewModelBase eventsWorkspaceViewModel;
        public ViewModelBase EventsWorkspaceViewModel
        {
            get => eventsWorkspaceViewModel;
            set
            {
                eventsWorkspaceViewModel = value;
                FirePropertyChanged(nameof(EventsWorkspaceViewModel));
            }
        }

        private DataLoadingStatus eventsDataLoadingStatus;
        public DataLoadingStatus EventsDataLoadingStatus
        {
            get => eventsDataLoadingStatus;
            set
            {
                eventsDataLoadingStatus = value;
                FirePropertyChanged(nameof(EventsDataLoadingStatus));
            }
        }

        public void ResetDataViewModel()
        {
            Entity = AtlasStatusEntity.Default();
            GeoDataWorkspaceViewModel = default;
            GeoDataLoadingStatus = DataLoadingStatus.NeverLoadedWithThisEntity;
            EventsWorkspaceViewModel = default;
            EventsDataLoadingStatus = DataLoadingStatus.NeverLoadedWithThisEntity;
            FilteringDateTime = DateTime.Now;
        }

        private bool CanQueryGeoDataInformationDoCommand() => NoGeoDataProcessingNow;

        private bool CanQueryEventsDataInformationDoCommand() => NoEventsDataProcessingNow;

        private void SwitchGeoDataToLoadingView() =>
            GeoDataWorkspaceViewModel = componentContext.Resolve<DataLoadingViewModel>();

        private void SwitchGeoDataToDataSourceView() =>
            GeoDataWorkspaceViewModel = componentContext.Resolve<AtlasGeoDataViewModel>();

        private void SwitchGeoDataToEmptyView() =>
            GeoDataWorkspaceViewModel = componentContext.Resolve<EmptyDataViewModel>();

        private void SwitchEventsDataToLoadingView() =>
            EventsWorkspaceViewModel = componentContext.Resolve<DataLoadingViewModel>();

        private void SwitchEventsDataToDataSourceView() =>
            EventsWorkspaceViewModel = componentContext.Resolve<AtlasEventsDataViewModel>();

        private void SwitchEventsDataToEmptyView() =>
            EventsWorkspaceViewModel = componentContext.Resolve<EmptyDataViewModel>();

        private async Task QueryGeoDataInformationAsyncCommand()
        {
            if (string.IsNullOrWhiteSpace(Entity.DeviceId))
                return;

            var messageBoxProvider = componentContext.Resolve<IMessageBoxProvider>();
            GeoDataLoadingStatus = DataLoadingStatus.LoadingNow;
            SwitchGeoDataToLoadingView();

            var mainViewModel = componentContext.Resolve<MainViewModel>();
            using (var unit = applicationWorkspace.CreateTracingWork())
            {
                bool resultNotEmpty = false;

                try
                {
                    mainViewModel.ExternalOperation = true;
                    var result = await mediator.Send(SerialNumberForGeoDataQueryModel.CreateQueryModel(Entity.Id), unit.Token);
                    resultNotEmpty = !result.IsDefault;

                    if (resultNotEmpty)
                    {
                        await mediator.Publish(new AtlasGeoDataReadyNotify(result), unit.Token);
                        if (!unit.Token.IsCancellationRequested)
                            SwitchGeoDataToDataSourceView();
                    }
                    else
                    {
                        SwitchGeoDataToEmptyView();
                    }
                }
                catch(TaskCanceledException)
                {
                    SwitchGeoDataToEmptyView();
                }
                catch(Exception ex)
                {
                    SwitchGeoDataToEmptyView();
                    await messageBoxProvider.ShowMessageAsync("Ошибка", $"Внутренняя ошибка приложения: {ex.Message}.");
                }

                if (!unit.Token.IsCancellationRequested)
                {
                    if (!resultNotEmpty)
                        GeoDataLoadingStatus = DataLoadingStatus.NeverLoadedWithThisEntity;
                    else
                        GeoDataLoadingStatus = DataLoadingStatus.CanRefresh;
                }

                mainViewModel.ExternalOperation = false;
            }
        }

        private async Task QueryEventsDataInformationAsyncCommand()
        {
            if (string.IsNullOrWhiteSpace(Entity.DeviceId))
                return;

            var messageBoxProvider = componentContext.Resolve<IMessageBoxProvider>();
            EventsDataLoadingStatus = DataLoadingStatus.LoadingNow;
            SwitchEventsDataToLoadingView();

            var mainViewModel = componentContext.Resolve<MainViewModel>();
            using (var unit = applicationWorkspace.CreateTracingWork())
            {
                bool resultNotEmpty = false;

                try
                {
                    mainViewModel.ExternalOperation = true;
                    var result = await mediator.Send(new SerialNumberForEventsQueryModel(Entity.DeviceId, FilteringDateTime), unit.Token);
                    resultNotEmpty = result.Any();

                    if (resultNotEmpty)
                    {
                        await mediator.Publish(new AtlasEventsReadyNotify(result), unit.Token);
                        if (!unit.Token.IsCancellationRequested)
                            SwitchEventsDataToDataSourceView();
                    }
                    else
                    {
                        SwitchEventsDataToEmptyView();
                    }
                }
                catch (TaskCanceledException)
                {
                    SwitchEventsDataToEmptyView();
                }
                catch (Exception ex)
                {
                    SwitchEventsDataToEmptyView();
                    await messageBoxProvider.ShowMessageAsync("Ошибка", $"Внутренняя ошибка приложения: {ex.Message}.");
                }

                if(!unit.Token.IsCancellationRequested)
                {
                    if (resultNotEmpty)
                        EventsDataLoadingStatus = DataLoadingStatus.NeverLoadedWithThisEntity;
                    else
                        EventsDataLoadingStatus = DataLoadingStatus.CanRefresh;
                }

                mainViewModel.ExternalOperation = false;
            }
        }
    }
}
