﻿using System;

namespace AST.Infrastructure.Bussiness
{
    public interface ITimerFactory
    {
        ITimer CreateTimer(Action action, int timeout);
    }
}
