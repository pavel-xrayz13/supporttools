﻿using Autofac;
using System;
using System.Reflection;

namespace AST.Infrastructure
{
    public static class ContainerBuilderExtensions
    {
        public static void RegisterAllMediatrHandlers(this ContainerBuilder self)
        {
            if (self == null)
                throw new NullReferenceException(nameof(self));

            var dataAccess = Assembly.GetExecutingAssembly();

            // All request handlers
            self.RegisterAssemblyTypes(dataAccess)
                   .Where(t => t.Namespace.Contains("Mediator"))
                   .Where(t => t.Name.EndsWith("MediatorHandler"))
                   .AsImplementedInterfaces().SingleInstance();

            // All notifiers
            self.RegisterAssemblyTypes(dataAccess)
                .Where(t => t.Namespace.Contains("Mediator"))
                .Where(t => t.Name.EndsWith("MediatorNotifier"))
                .AsImplementedInterfaces().SingleInstance();
        }
    }
}
