﻿using System;
using System.Windows;
using System.Windows.Threading;
using Autofac;
using MahApps.Metro.Controls;
using MediatR;
using AST.Infrastructure;
using AST.Infrastructure.Dal;
using AST.Infrastructure.Dal.MySql;
using AST.Infrastructure.Dal.Dapper;
using AST.Infrastructure.Bussiness.ViewModels;
using AST.Presentation.AppImpl;
using AST.Presentation.Views;
using AST.Infrastructure.Bussiness.GeoData;
using AST.Infrastructure.Bussiness;

namespace AST.Presentation
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            var window = new MainWindow
            {
                Title = $"Author Support Tools (версия {EnvironmentInfo.Version})"
            };

            IocKernel.Default.Container = InitializeDependencyContainer(window);
            window.DataContext = IocKernel.Default.Container.Resolve<MainViewModel>();
            window.Show();
        }

        private IContainer InitializeDependencyContainer(MainWindow window)
        {
            var containerBuilder = new ContainerBuilder();
            containerBuilder.Register(c => window).As<MetroWindow>().SingleInstance();
            containerBuilder.Register(c => window.Dispatcher).As<Dispatcher>().SingleInstance();
            BuildMediatrServiceFactory(containerBuilder);
            containerBuilder.RegisterType<MahAppsMessageBoxProvider>().As<IMessageBoxProvider>().As<IErrorHandler>().SingleInstance();
            containerBuilder.RegisterType<WpfTimerFactory>().As<ITimerFactory>().SingleInstance();
            containerBuilder.RegisterType<MySqlDbConnector>().As<IDbConnector>().SingleInstance();
            containerBuilder.RegisterType<DapperAtlasStatusRepository>().As<IAtlasStatusRepository>().SingleInstance();
            containerBuilder.RegisterType<ApplicationWorkspace>().As<IApplicationWorkspace>().SingleInstance();
            containerBuilder.RegisterAllMediatrHandlers();
            containerBuilder.RegisterType<DefaultGeoDataService>().As<IGeoDataService>();
            containerBuilder.RegisterType<MainViewModel>().As<MainViewModel>().SingleInstance();
            containerBuilder.RegisterType<EmptyDataViewModel>().SingleInstance();
            containerBuilder.RegisterType<DataLoadingViewModel>().SingleInstance();
            containerBuilder.RegisterType<AtlasStatusViewModel>().SingleInstance();
            containerBuilder.RegisterType<AtlasGeoDataViewModel>().SingleInstance();
            containerBuilder.RegisterType<AtlasEventsDataViewModel>().SingleInstance();
            return containerBuilder.Build();
        }

        private void BuildMediatrServiceFactory(ContainerBuilder builder)
        {
            builder.RegisterType<Mediator>().As<IMediator>().SingleInstance();
            builder.Register<ServiceFactory>(ctx =>
            {
                var context = ctx.Resolve<IComponentContext>();
                return serviceType =>
                {
                    try
                    {
                        return context.Resolve(serviceType);
                    }
                    catch (Exception)
                    {
                        return Array.CreateInstance(serviceType.GenericTypeArguments[0], 0);
                    }
                };
            });
        }
    }
}
