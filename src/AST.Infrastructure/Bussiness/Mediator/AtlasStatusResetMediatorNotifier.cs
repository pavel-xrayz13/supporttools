﻿using AST.Infrastructure.Bussiness.ViewModels;
using Autofac;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace AST.Infrastructure.Bussiness.Mediator
{
    public class AtlasStatusResetNotify : INotification
    {
        public static AtlasStatusResetNotify Default { get; } = new AtlasStatusResetNotify();

        // всегда используем Default, так как не аргументов заодно не "спамим" GC.
        private AtlasStatusResetNotify()
        {}
    }

    public class AtlasStatusResetMediatorNotifier : INotificationHandler<AtlasStatusResetNotify>
    {
        private readonly IComponentContext componentContext;

        public AtlasStatusResetMediatorNotifier(IComponentContext componentContext)
        {
            this.componentContext = componentContext ?? throw new ArgumentNullException(nameof(componentContext));
        }

        public Task Handle(AtlasStatusResetNotify notification, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var statusViewModel = componentContext.Resolve<AtlasStatusViewModel>();
            statusViewModel.ResetDataViewModel();
            return Task.CompletedTask;
        }
    }
}
