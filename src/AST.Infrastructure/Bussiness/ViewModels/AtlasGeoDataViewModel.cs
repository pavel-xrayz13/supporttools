﻿using AST.Infrastructure.Bussiness.GeoData;
using AST.Infrastructure.Domain.Entities;
using System;

namespace AST.Infrastructure.Bussiness.ViewModels
{
    public class AtlasGeoDataViewModel : ChainingEntityViewModelBase<AtlasGeoDataEntity>
    {
        private readonly IGeoDataService geoDataService;

        public AtlasGeoDataViewModel(IGeoDataService geoDataService)
        {
            this.geoDataService = geoDataService ?? throw new ArgumentNullException(nameof(geoDataService));

            WhenFireProperty(nameof(GeoData))
                .ThenFireProperty(nameof(GeoLinkYandex))
                .ThenFireProperty(nameof(GeoLinkGoogle));
        }

        public string GeoLinkYandex => geoDataService.PointOnMapUri(GeoDataProvider.Yandex, Entity).ToString();
        public string GeoLinkGoogle => geoDataService.PointOnMapUri(GeoDataProvider.Google, Entity).ToString();
    }
}
