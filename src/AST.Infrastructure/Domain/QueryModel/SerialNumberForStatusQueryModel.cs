﻿using AST.Infrastructure.Domain.Entities;
using MediatR;
using System;

namespace AST.Infrastructure.Domain.QueryModel
{
    public class SerialNumberForStatusQueryModel : IRequest<AtlasStatusEntity>
    {
        public string Serial { get; }

        protected SerialNumberForStatusQueryModel(string serial)
        {
            if (string.IsNullOrWhiteSpace(serial))
                throw new ArgumentException($"{nameof(serial)} parameter cannot be null, empty or whitespace");

            Serial = serial;
        }

        public static SerialNumberForStatusQueryModel CreateQueryModel(string serial) => 
            new SerialNumberForStatusQueryModel(serial);
    }
}
