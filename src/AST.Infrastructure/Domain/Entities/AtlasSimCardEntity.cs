﻿using System;

namespace AST.Infrastructure.Domain.Entities
{
    public class AtlasSimCardEntity
    {
        public string Card { get; }
        public string Iccid { get; }
        public decimal LastBalanceValue { get; }
        public DateTime LastBalanceCheck { get; }

        public AtlasSimCardEntity(string card, string iccid, decimal balance, DateTime lastCheck)
        {
            Card = card;
            Iccid = iccid;
            LastBalanceValue = balance;
            LastBalanceCheck = lastCheck;
        }

        public static AtlasSimCardEntity Default() => new AtlasSimCardEntity(string.Empty, string.Empty, default, default);
    }
}
