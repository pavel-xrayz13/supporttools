﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using AST.Infrastructure.Bussiness.ViewModels.Internals;

namespace AST.Presentation.Converters
{
    internal class SerialNumberNotificationBadgeTooltipConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var enumeration = (SerialNumberNotificationBadge)value;
            switch(enumeration)
            {
                case SerialNumberNotificationBadge.NoBadge:
                    return LoadNoBadgeText();
                case SerialNumberNotificationBadge.QueryIsRunning:
                    return LoadQueryIsRunningText();
                case SerialNumberNotificationBadge.QueryTakesTooMuchTime:
                    return LoadQueryTakesTooMuchTimeText();
                default:
                    throw new InvalidOperationException();
            }
        }

        public virtual object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        private string LoadQueryTakesTooMuchTimeText()
        {
            var resource = Application.Current.TryFindResource("MainWindowNotifyQueryTakesTooMuchTimeText");
            if (resource == null)
                return string.Empty;
            return resource.ToString();
        }

        private string LoadNoBadgeText()
        {
            var resource = Application.Current.TryFindResource("MainWindowNotifySetSerialAndPressEnter");
            if (resource == null)
                return string.Empty;
            return resource.ToString();
        }

        private string LoadQueryIsRunningText()
        {
            var resource = Application.Current.TryFindResource("MainWindowNotifyLoadQueryIsRunningText");
            if (resource == null)
                return string.Empty;
            return resource.ToString();
        }
    }
}
