﻿using System;

namespace AST.Infrastructure.Domain.Entities
{
    public class AtlasEventEntity
    {
        public DateTime Created { get; }
        public AtlasEventsDefinition Definition { get; }
        public byte[] Data { get; }
        public bool IsAlarming { get; }

        public AtlasEventEntity(DateTime created, AtlasEventsDefinition definition, byte[] data, bool isAlarm)
        {
            Created = created;
            Definition = definition;
            Data = data;
            IsAlarming = isAlarm;
        }

        public static AtlasEventEntity Default() => new AtlasEventEntity(default, default, default, default);

        public bool IsDefault => Definition == AtlasEventsDefinition.Unknown &&
            Created == default && IsAlarming == default;
    }
}
