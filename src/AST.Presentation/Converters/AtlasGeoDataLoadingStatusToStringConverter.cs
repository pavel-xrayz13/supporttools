﻿using AST.Infrastructure.Bussiness.ViewModels.Internals;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace AST.Presentation.Converters
{
    internal class AtlasGeoDataLoadingStatusToStringConverter : IValueConverter
    {
        private static string atlasGeoDataStatusCanRefreshText = default;
        private static string atlasGeoDataStatusLoadingNowText = default;
        private static string atlasGeoDataStatusNeverLoadedWithThisEntityText = default;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var enumeration = (DataLoadingStatus)value;
            switch (enumeration)
            {
                case DataLoadingStatus.CanRefresh:
                    return StatusCanRefreshText();
                case DataLoadingStatus.LoadingNow:
                    return StatusLoadingNowText();
                case DataLoadingStatus.NeverLoadedWithThisEntity:
                    return StatusNeverLoadedWithThisEntityText();
                default:
                    throw new InvalidOperationException();
            }
        }

        public virtual object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        private string StatusCanRefreshText()
        {
            if (atlasGeoDataStatusCanRefreshText != default)
                return atlasGeoDataStatusCanRefreshText;

            var resource = Application.Current.TryFindResource("AtlasGeoDataStatusCanRefreshText");
            if (resource == null)
                atlasGeoDataStatusCanRefreshText = string.Empty;
            else
                atlasGeoDataStatusCanRefreshText = resource.ToString();

            return atlasGeoDataStatusCanRefreshText;
        }

        private string StatusLoadingNowText()
        {
            if (atlasGeoDataStatusLoadingNowText != default)
                return atlasGeoDataStatusLoadingNowText;

            var resource = Application.Current.TryFindResource("AtlasGeoDataStatusLoadingNowText");
            if (resource == null)
                atlasGeoDataStatusLoadingNowText = string.Empty;
            else
                atlasGeoDataStatusLoadingNowText = resource.ToString();

            return atlasGeoDataStatusLoadingNowText;
        }

        private string StatusNeverLoadedWithThisEntityText()
        {
            if (atlasGeoDataStatusNeverLoadedWithThisEntityText != default)
                return atlasGeoDataStatusNeverLoadedWithThisEntityText;

            var resource = Application.Current.TryFindResource("AtlasGeoDataStatusNeverLoadedWithThisEntityText");
            if (resource == null)
                atlasGeoDataStatusNeverLoadedWithThisEntityText = string.Empty;
            else
                atlasGeoDataStatusNeverLoadedWithThisEntityText = resource.ToString();

            return atlasGeoDataStatusNeverLoadedWithThisEntityText;
        }
    }
}
