﻿using System.Threading.Tasks;

namespace AST.Infrastructure
{
    public interface IMessageBoxProvider
    {
        Task ShowMessageAsync(string header, string message);
        Task<bool> ShowConfirmationAsync(string header, string message);
    }
}
