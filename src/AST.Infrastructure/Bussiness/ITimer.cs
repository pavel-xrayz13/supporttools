﻿namespace AST.Infrastructure.Bussiness
{
    public interface ITimer
    {
        void Start();
        void Stop();
    }
}
