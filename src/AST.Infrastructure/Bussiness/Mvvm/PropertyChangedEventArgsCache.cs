﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace AST.Infrastructure.Bussiness.Mvvm
{
    public sealed class PropertyChangedEventArgsCache
    {
        /// <summary>
        /// The underlying dictionary. This instance is its own mutex.
        /// </summary>
        private readonly Dictionary<string, PropertyChangedEventArgs> cache = new Dictionary<string, PropertyChangedEventArgs>();

        /// <summary>
        /// Private constructor to prevent other instances.
        /// </summary>
        private PropertyChangedEventArgsCache()
        {
        }

        /// <summary>
        /// The global instance of the cache.
        /// </summary>
        public static PropertyChangedEventArgsCache Instance { get; } = new PropertyChangedEventArgsCache();

        /// <summary>
        /// Retrieves a <see cref="PropertyChangedEventArgs"/> instance for the specified property, creating it and adding it to the cache if necessary.
        /// </summary>
        /// <param name="propertyName">The name of the property that changed.</param>
        public PropertyChangedEventArgs Get(string propertyName)
        {
            if (string.IsNullOrWhiteSpace(propertyName))
                throw new ArgumentException("null , empty or whitespace string argument", nameof(propertyName));

            lock (cache)
            {
                if (cache.TryGetValue(propertyName, out PropertyChangedEventArgs result))
                    return result;

                result = new PropertyChangedEventArgs(propertyName);
                cache.Add(propertyName, result);
                return result;
            }
        }
    }
}
