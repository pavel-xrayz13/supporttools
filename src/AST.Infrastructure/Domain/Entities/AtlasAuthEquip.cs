﻿namespace AST.Infrastructure.Domain.Entities
{
    public enum AtlasAuthEquip
    {
        PinOnly,
        Extended
    }
}
