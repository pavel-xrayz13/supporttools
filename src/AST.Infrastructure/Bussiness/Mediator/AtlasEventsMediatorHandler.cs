﻿using AST.Infrastructure.Dal;
using AST.Infrastructure.Domain.Entities;
using AST.Infrastructure.Domain.QueryModel;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace AST.Infrastructure.Bussiness.Mediator
{
    public class AtlasEventsMediatorHandler : IRequestHandler<SerialNumberForEventsQueryModel, IEnumerable<AtlasEventEntity>>
    {
        private readonly IAtlasStatusRepository atlasStatusRepository;

        public AtlasEventsMediatorHandler(IAtlasStatusRepository atlasStatusRepository)
        {
            this.atlasStatusRepository = atlasStatusRepository ?? 
                throw new ArgumentNullException(nameof(atlasStatusRepository));
        }

        public Task<IEnumerable<AtlasEventEntity>> Handle(SerialNumberForEventsQueryModel request, CancellationToken cancellationToken)
        {
            var eventsFilter = new GetAtlasEventsFilter()
            {
                StartDate = request.Date.DateTimeToBeginDay(),
                EndDate = request.Date.DateTimeToEndDay()
            };

            return atlasStatusRepository.GetAtlasEventsByFilter(request.Serial, eventsFilter, cancellationToken);
        }
    }
}
