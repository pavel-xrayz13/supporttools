﻿using System;
using System.Threading.Tasks;

namespace AST.Infrastructure
{
    public interface IErrorHandler
    {
        Task HandleErrorAsync(Exception ex);
    }
}
