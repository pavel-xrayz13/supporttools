﻿namespace AST.Infrastructure.Bussiness.ViewModels.Internals
{
    public enum SerialNumberNotificationBadge
    {
        NoBadge,
        QueryIsRunning,
        QueryTakesTooMuchTime
    }
}
