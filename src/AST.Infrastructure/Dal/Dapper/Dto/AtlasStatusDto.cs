﻿using System;

namespace AST.Infrastructure.Dal.Dapper.Dto
{
    public class AtlasStatusDto
    {
        public int Id { get; set; }
        public string DeviceId { get; set; }
        public string DeviceStatus { get; set; }
        public string SimCardType { get; set; }
        public string Iccid { get; set; }
        public decimal SimCardLastBalanceValue { get; set; }
        public DateTime SimCardLastBalanceCheck { get; set; }
        public string AuthEquip { get; set; }
        public string AuthImmoPin { get; set; }
        public string AuthImmoTag { get; set; }
        public string AuthSlave { get; set; }
    }
}
