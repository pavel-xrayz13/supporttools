﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace AST.Infrastructure.Domain.Entities
{
    public enum AtlasEventsDefinition : byte
    {
        Unknown = 0x00,
        ResetToFactory = 0x01, // Reset To Factory Settings (заложено но не реализовано)
        JammingDetection = 0x08, // Jamming detection (заложено но не реализовано)
        RoamingEvent = 0x09, // Событие роуминга
        SystemReset = 0x0A, // System Reset
        TimeSync = 0x0B, // Timer Syncronization Event
        FirmwareUpdate = 0x0C, // Событие обновления прошивки
        LogLoading = 0x0D, // Событие загрузки логов
        LogCleanup = 0x0E, // Событие очистки логов
        UssdRequest = 0x0F, // Завершения USSD-запроса
        IgnitionInputChange = 0x20, // Изменение входа зажигания
        AlarmInputChange = 0x21, // Изменение входа тревожная кнопка
        AutostartStatusInputChange = 0x22, // Изменение входа статуса автозапуска (IN2)
        UniversalEventStatusChange = 0x23, // Изменение состояния входа (универсальное событие)
        AutostartEvent = 0x30, // Событие автозапуска
        TemperatureStatus = 0x40, // Статус: температура
        BatteryVoltageStatus = 0x41, // Статус: напряжение аккумулятора
        ImmoChangeStatus = 0x50, // Изменение состояния ИММО
        SecurityChangeToEnabled = 0x60, // Постановка на охрану
        SecurityChangeToDisabled = 0x61, // Снятие с охраны
        Alert = 0x62, // Тревога
        CentralLocking = 0x63, // ЦЗ
        AntihijackDisable = 0x64, // Отключение функции антиограбления
        NewKeyFlashing = 0x65, // Прописывание нового ключа
        TagRewrite = 0x66, // Перезапись меток
        SmartphoneTag = 0x67, // Смартфон-метка
        ServiceMode = 0x68, // Сервисный режим
        PinChangeMode = 0x69, // Переход в режим смены PIN
        AuthStatus = 0x70, // Статус авторизации
        Ignition = 0x80, // Зажигание
        Engine = 0x81, // Двигатель
        LowBatteryInTag = 0x82, // Низкий заряд АКБ в метке
        DriverDoor = 0x83, // Водительская дверь
        PassengerDoor = 0x84, // Пассажирская дверь
        RearLeftDoor = 0x85, // Задняя левая дверь
        ReadRightDoor = 0x86, // Задняя правая дверь
        PassengerDoors = 0x87, // Пассажирские двери (для машин, различающих только водительскую и пассажирские двери)
        Trunk = 0x88, // Багажник
        Bonnet = 0x89 // Капот
    }

    public enum AtlasEventResetToFactorySettings : byte
    {
        HardwareReset = 0x00, // Аппаратный сброс
        UsbCommandReset = 0x01, // Сброс командой по USB
        MobileApplicationReset = 0x02 // Сброс через мобильное приложение
    }

    public enum AtlasEventRoamingEvent : byte
    {
        HomeNet = 0x01, // устройство в домашней сети.
        RoamingNet = 0x00 // устройство в роуминговой сети.
    }

    public enum AtlasEventSystemReset : byte
    {
        ReasonLowPwr = 0x01, // ???
        ReasonWindowWdg = 0x02, // ???
        ReasonIwdg = 0x03, // ???
        ReasonSoft = 0x04, // ???
        ReasonPor = 0x05, // ???
        ReasonPstPin = 0x06, // ???
        ReasonBor = 0x07 // ???
    }

    public enum AtlasEventFirmwareUpdate : byte
    {
        DownloadSuccessful, // Прошивка успешно загружена во внешнюю Flash, далее последует перезагрузка и обновление
        Busy, // Процесс занят (уже выполняется)
        Fail, // Ошибка выполнения
        FailHWVersionMismath, // Аппаратная версия прошивки не совместима
        AppVersionSame, // Версии установленной и загружаемой прошивок идентичны
        SignatureBad, // Неверная сигнатура файла
        IOError, // Ошибка записи или чтения во Flash
        CRCResourceFail, // Ошибка CRC ресурса
        CRCInvalid, // Ошибка CRC
        InvalidLength, // Неверная длина файла
        SessionCfgFail, // ошибка конфигурации сессии
        SessionStartFail, // Ошибка инициализации сессии
        FileNotFound, // Файл с указанным именем не найден на сервере
        UserInvalid, // Неверный логин
        PasswordInvalid, // Неверный пароль
        ConnectionError, // ошибка соединения
        ServerNotFound, // Сервер не найден
        NetworkError, // Сетевая ошибка
        DownloadError, // Ошибка загрузки
        UnknownError, // Неизвестная ошибка
        InternetFail, // Интернет отсутствует
        UpdateSuccessful, // Обновление ПО прошло успешно
        UpdateFail, // Ошибка обновления ПО
        ReceiveTimeout, // Таймаут ожидания данных прошивки
        FtpClientBusy, // FTP клиент занят
        ConnectionTimeout // Таймаут соединения
    }

    public enum AtlasEventLogLoading : byte
    {
        StartLogLoading = 0x00, // Запуск загрузки логов
        LogLoadingSuccessful = 0x01, // Успешное завершение загрузки логов
        ConfigurationError = 0x02, // Ошибка конфигурации
        StartupError = 0x03, // Ошибка запуска
        LogEntryError = 0x04, // Ошибка номера лога
        FolderCreationError = 0x05, // Ошибка создания папки
        WriteError = 0x06 // Ошибка записи
    }

    public enum AtlasEventLogCleanup : byte
    {
        Successful = 0x00, // Очистка логов успешно завершена
        Error = 0x01 // Ошибка очистки логов
    }

    public enum AtlasEventUssdRequest : byte
    {
        Successful = 0x00, // запрос успешно выполнен
        TimeoutFailure = 0xF0, // Ошибка: таймаут ожидания ответа
        RequestAborted = 0xF1, // Ошибка: запрос был отменен
        UnknownReason = 0xFE, // Неизвестная ошибка
        NAK = 0xFF
    }

    public enum AtlasEventIgnitionInputChange : byte
    {
        IgnitionOff = 0x00, // зажигание отключено
        IgnitionOn = 0x01 // зажигание включено
    }

    public enum AtlasEventAlarmInputChange : byte
    {
        ButtonUnpushed = 0x00, // кнопка отпущена
        ButtonPushed = 0x01 // кнопка нажата
    }

    public enum AtlasEventAutostartStatusInputChange : byte
    {
        InputDeactivated = 0x00, // вход неактивен
        InputActive = 0x01 // вход активен
    }

    public enum AtlasEventUniversalEventPortStatus : byte
    {
        PortDeactivated = 0x00, // вход неактивен
        PortActive = 0x01 // вход активен
    }

    public enum AtlasEventUniversalEventPortChange : byte
    {
        PortDeactivated = 0x00, // вход неактивен
        PortActive = 0x01 // вход активен
    }

    public enum AtlasEventAutostart : byte
    {
        EngineStarted = 0x01, // двигатель запущен
        AutostartDoneEngineShutOff = 0x02, // автозапуск завершен, двигатель заглушен (завелся, но не проработал нужное время)
        AutostartDoneIgnitionHook = 0x11, // автозапуск завершен, перехват зажигания
        AutostartDonePowerReset = 0x12, // автозапуск завершен, сброс питания
        AutostartDoneByCommand = 0x13, // Автозапуск завершен: по команде
        AutostartDoneByTimeout = 0x14, // Автозапуск завершен: по таймауту
        AutostartDoneByError = 0x15, // Автозапуск завершен: ошибка автозапуска
        AutostartPerimeterTrespassing = 0x20, // Автозапуск завершен: нарушение периметра. Код события зарезервирован блоком AUTOSTART
        AutostartActivateByBlock = 0x30, // Автозапуск активирован блоком Autostart (содержит два аргумента – длительность запуска и текущее значение таймера в минутах)
        FailedToStartNoRetries = 0xF0, // не удалось выполнить запуск, исчерпаны попытки
        FailedToStartNoIglaConnection = 0xF1, // автозапуск завершен, нет связи с IGLA
        InvalidWiring = 0xF2, // некорректное подключение (отсутствует обратная связь)
        AutostartDoneInvalidEngineResponse = 0xF3, // Автозапуск завершен, получен неверный статус двигателя
        FailureNoModuleConnection = 0xF4, // Ошибка автозапуска – потеряна связь с модулем
    }

    public enum AtlasEventAutostartActivateByBlockData
    {
        StartDuration = 1, // длительность запуска
        CurrentTimer = 2, // текущее значение таймера в минутах
    }

    public enum AtlasEventSecurityChange : byte
    {
        ByKeyFob = 0x01, // со штатного брелока
        ByHandsFree = 0x02, // с hands free
    }

    public enum AtlasEventAlert : byte
    {
        Generic = 0x01, // срабатывание штатки
        IglaImmo = 0x02, // срабатывание иммо
        AntiHiJack = 0x03, // срабатывание антиограбления
        LowHitSensor = 0x10, // срабатывание датчика слабого удара
        HighHitSensor = 0x11, // срабатывание датчика сильного удара
        MovementSensor = 0x12, // срабатывание датчика движения
        TiltSensor = 0x13, // срабатывание датчика наклона
        SecurityZone = 0x14 // Тревога зоны охраны
    }

    public enum AtlasEventCentralLocking : byte
    {
        Opened = 0x00, // открыт
        Closed = 0x01 // закрыт
    }

    public enum AtlasEventSmartphoneTag : byte
    {
        Attached = 0x01, // отвязка
        Detached = 0x00 // привязка
    }

    public enum AtlasEventServiceMode : byte
    {
        On = 0x01, // переход в режим
        Off = 0x00 // выход из режима
    }

    public enum AtlasEventTriggerState : byte
    {
        On = 0x01, // включено
        Off = 0x00, // выключено
    }

    public class AtlasAuthState
    {
        private static readonly int AuthByTagActualBit = 3;
        private static readonly int AuthByTagBit = 2;
        private static readonly int AuthByPinActualBit = 1;
        private static readonly int AuthByPinBit = 0;

        public bool AuthByTagActual { get; }
        public bool AuthByTag { get; }
        public bool AuthByPinActual { get; }
        public bool AuthByPin { get; }

        public AtlasAuthState(byte data)
        {
            var bitArray = new BitArray(new[] { data });
            AuthByTagActual = bitArray[AuthByTagActualBit];
            AuthByTag = bitArray[AuthByTagBit];
            AuthByPinActual = bitArray[AuthByPinActualBit];
            AuthByPin = bitArray[AuthByPinBit];
        }

        public override string ToString() =>
            $"{Convert.ToInt32(AuthByTagActual)}{Convert.ToInt32(AuthByTag)}{Convert.ToInt32(AuthByPinActual)}{Convert.ToInt32(AuthByPin)}";
    }

    public class AtlasLowBatteryTagState
    {
        private static readonly int TagOneBit = 0;
        private static readonly int TagTwoBit = 1;

        public bool TagOne { get; } // состояние метки 1
        public bool TagTwo { get; } // состояние метки 2

        public AtlasLowBatteryTagState(byte data)
        {
            var bitArray = new BitArray(new[] { data });
            TagOne = bitArray[TagOneBit];
            TagTwo = bitArray[TagTwoBit];
        }

        public override string ToString() =>
            $"{Convert.ToInt32(TagOne)}{Convert.ToInt32(TagTwo)}";
    }

    public static class AtlasEventDefinitionExtensions
    {
        private static readonly IEnumerable<AtlasEventsDefinition> triggerStatusFilter = new List<AtlasEventsDefinition>
        {
            AtlasEventsDefinition.Ignition,
            AtlasEventsDefinition.Engine,
            AtlasEventsDefinition.DriverDoor,
            AtlasEventsDefinition.PassengerDoor,
            AtlasEventsDefinition.PassengerDoors,
            AtlasEventsDefinition.RearLeftDoor,
            AtlasEventsDefinition.ReadRightDoor,
            AtlasEventsDefinition.Trunk,
            AtlasEventsDefinition.Bonnet
        };

        public static AtlasEventResetToFactorySettings AsAtlasEventResetToFactorySettings(this AtlasEventsDefinition self, byte[] data) =>
            AsAtlasEnumerationStatusForSingleByte<AtlasEventResetToFactorySettings>(self, AtlasEventsDefinition.ResetToFactory, data);

        public static AtlasEventRoamingEvent AsAtlasEventRoamingEvent(this AtlasEventsDefinition self, byte[] data) =>
            AsAtlasEnumerationStatusForSingleByte<AtlasEventRoamingEvent>(self, AtlasEventsDefinition.RoamingEvent, data);

        public static AtlasEventSystemReset AsAtlasEventSystemResetEvent(this AtlasEventsDefinition self, byte[] data) =>
            AsAtlasEnumerationStatusForSingleByte<AtlasEventSystemReset>(self, AtlasEventsDefinition.SystemReset, data);

        public static AtlasEventFirmwareUpdate AsAtlasEventFirmwareUpdateEvent(this AtlasEventsDefinition self, byte[] data) =>
            AsAtlasEnumerationStatusForSingleByte<AtlasEventFirmwareUpdate>(self, AtlasEventsDefinition.FirmwareUpdate, data);

        public static uint AsAtlasTimeSync(this AtlasEventsDefinition self, byte[] data)
        {
            if (self != AtlasEventsDefinition.TimeSync)
                throw new InvalidOperationException();
            if (data.Length < 4) // sizeof(uint32)
                throw new InvalidOperationException();

            return BitConverter.ToUInt32(data, 0);
        }

        public static AtlasEventLogLoading AsAtlasEventLogLoadingEvent(this AtlasEventsDefinition self, byte[] data) =>
            AsAtlasEnumerationStatusForSingleByte<AtlasEventLogLoading>(self, AtlasEventsDefinition.LogLoading, data);

        public static Tuple<AtlasEventLogCleanup, uint> AsAtlasLogCleanup(this AtlasEventsDefinition self, byte[] data)
        {
            if (self != AtlasEventsDefinition.LogCleanup)
                throw new InvalidOperationException();
            if (data.Length < 5) // sizeof(uint8) + sizeof(uint32)
                throw new InvalidOperationException();

            var cleanupData = (AtlasEventLogCleanup)data[0];
            var mask = BitConverter.ToUInt32(data, 1);

            return Tuple.Create(cleanupData, mask);
        }

        public static AtlasEventUssdRequest AsAtlasUssdRequest(this AtlasEventsDefinition self, byte[] data) =>
            AsAtlasEnumerationStatusForSingleByte<AtlasEventUssdRequest>(self, AtlasEventsDefinition.UssdRequest, data);

        public static AtlasEventIgnitionInputChange AsAtlasIgnitionInputChange(this AtlasEventsDefinition self, byte[] data) =>
            AsAtlasEnumerationStatusForSingleByte<AtlasEventIgnitionInputChange>(self, AtlasEventsDefinition.IgnitionInputChange, data);

        public static AtlasEventAlarmInputChange AsAtlasAlarmInputChange(this AtlasEventsDefinition self, byte[] data) =>
            AsAtlasEnumerationStatusForSingleByte<AtlasEventAlarmInputChange>(self, AtlasEventsDefinition.AlarmInputChange, data);

        public static AtlasEventAutostartStatusInputChange AsAtlasAutostartStatusInputChange(this AtlasEventsDefinition self, byte[] data) =>
            AsAtlasEnumerationStatusForSingleByte<AtlasEventAutostartStatusInputChange>(self, AtlasEventsDefinition.AutostartStatusInputChange, data);

        public static Tuple<AtlasEventUniversalEventPortStatus[], AtlasEventUniversalEventPortChange[]> AsAtlasUniversalEventStatusChange(
            this AtlasEventsDefinition self, byte[] data)
        {
            const int PortCount = 8;

            if (self != AtlasEventsDefinition.UniversalEventStatusChange)
                throw new InvalidOperationException();
            if (data.Length < 2) // sizeof(uint8) + sizeof(uint8)
                throw new InvalidOperationException();

            var portStatus = new AtlasEventUniversalEventPortStatus[PortCount];
            var portChange = new AtlasEventUniversalEventPortChange[PortCount];

            var portStatusByte = new BitArray(data[0]);
            var portChangeByte = new BitArray(data[1]);

            for (var i = 0; i < portStatusByte.Length; ++i)
                portStatus[i] = (AtlasEventUniversalEventPortStatus)Convert.ToByte(portStatusByte[i]);
            for (var i = 0; i < portChangeByte.Length; ++i)
                portChange[i] = (AtlasEventUniversalEventPortChange)Convert.ToByte(portChangeByte[i]);

            return Tuple.Create(portStatus, portChange);
        }

        public static Tuple<byte, byte> AsAtlasUniversalEventStatusChangeRaw(this AtlasEventsDefinition self, byte[] data)
        {
            if (self != AtlasEventsDefinition.UniversalEventStatusChange)
                throw new InvalidOperationException();
            if (data.Length < 2) // sizeof(uint8) + sizeof(uint8)
                throw new InvalidOperationException();

            return Tuple.Create(data[0], data[1]);
        }

        public static AtlasEventAutostart AsAtlasEventAutostart(this AtlasEventsDefinition self, byte[] data) =>
            AsAtlasEnumerationStatusForSingleByte<AtlasEventAutostart>(self, AtlasEventsDefinition.AutostartEvent, data);

        public static byte AsAtlasEventAutoStartByteAtIndex(this AtlasEventAutostart self, byte[] data, 
            AtlasEventAutostartActivateByBlockData e)
        {
            if (self != AtlasEventAutostart.AutostartActivateByBlock)
                throw new InvalidOperationException();
            if (data.Length < 3) // sizeof(uint8) + 2*sizeof(uint8)
                throw new InvalidOperationException();

            return data[(int)e];
        }

        public static float AsAtlasEventTemperatureStatus(this AtlasEventsDefinition self, byte[] data)
        {
            if (self != AtlasEventsDefinition.TemperatureStatus)
                throw new InvalidOperationException();
            if (data.Length < 2) // sizeof(int16)
                throw new InvalidOperationException();

            var integer = BitConverter.ToInt16(data, 0);
            return (float)integer / 100;
        }

        public static float AsAtlasEventBatteryVoltageStatus(this AtlasEventsDefinition self, byte[] data)
        {
            if (self != AtlasEventsDefinition.BatteryVoltageStatus)
                throw new InvalidOperationException();
            if (data.Length < 2) // sizeof(uint16)
                throw new InvalidOperationException();

            var integer = BitConverter.ToUInt16(data, 0);
            return (float)integer / 100;
        }

        public static uint AsAtlasImmoChangeStatus(this AtlasEventsDefinition self, byte[] data)
        {
            if (self != AtlasEventsDefinition.ImmoChangeStatus)
                throw new InvalidOperationException();
            if (data.Length < 4) // sizeof(uint32)
                throw new InvalidOperationException();

            return BitConverter.ToUInt32(data, 0);
        }

        public static AtlasEventSecurityChange AsAtlasSecurityChangeToEnabled(this AtlasEventsDefinition self, byte[] data) =>
            AsAtlasEnumerationStatusForSingleByte<AtlasEventSecurityChange>(self, AtlasEventsDefinition.SecurityChangeToEnabled, data);

        public static AtlasEventSecurityChange AsAtlasSecurityChangeToDisabled(this AtlasEventsDefinition self, byte[] data) =>
            AsAtlasEnumerationStatusForSingleByte<AtlasEventSecurityChange>(self, AtlasEventsDefinition.SecurityChangeToDisabled, data);

        public static AtlasEventAlert AsAtlasEventAlert(this AtlasEventsDefinition self, byte[] data) =>
            AsAtlasEnumerationStatusForSingleByte<AtlasEventAlert>(self, AtlasEventsDefinition.Alert, data);

        public static AtlasEventCentralLocking AsAtlasEventCentralLocking(this AtlasEventsDefinition self, byte[] data) =>
            AsAtlasEnumerationStatusForSingleByte<AtlasEventCentralLocking>(self, AtlasEventsDefinition.CentralLocking, data);

        public static AtlasEventSmartphoneTag AsAtlasEventSmartphoneTag(this AtlasEventsDefinition self, byte[] data) =>
            AsAtlasEnumerationStatusForSingleByte<AtlasEventSmartphoneTag>(self, AtlasEventsDefinition.SmartphoneTag, data);

        public static AtlasEventServiceMode AsAtlasServiceMode(this AtlasEventsDefinition self, byte[] data) =>
            AsAtlasEnumerationStatusForSingleByte<AtlasEventServiceMode>(self, AtlasEventsDefinition.ServiceMode, data);

        public static AtlasAuthState AsAtlasAuthStatus(this AtlasEventsDefinition self, byte[] data)
        {
            if (self != AtlasEventsDefinition.AuthStatus)
                throw new InvalidOperationException();
            if (data.Length < 1) // sizeof(uint8)
                throw new InvalidOperationException();

            return new AtlasAuthState(data[0]);
        }

        public static AtlasEventTriggerState AsAtlasTriggerStatus(this AtlasEventsDefinition self, byte[] data) =>
            AsAtlasEnumerationStatusForSingleByteWithFunc<AtlasEventTriggerState>(self, x => triggerStatusFilter.Contains(x), data);

        public static AtlasLowBatteryTagState AsAtlasLowBatteryTagStatus(this AtlasEventsDefinition self, byte[] data)
        {
            if (!triggerStatusFilter.Contains(self))
                throw new InvalidOperationException();
            if (data.Length < 1)
                throw new InvalidOperationException();

            return new AtlasLowBatteryTagState(data[0]);
        }

        public static bool IsAlarming(this AtlasEventsDefinition self, byte[] data)
        {
            var result = false;
            switch(self)
            {
                case AtlasEventsDefinition.Alert:
                    result = true;
                    break;

                case AtlasEventsDefinition.NewKeyFlashing:
                    result = true;
                    break;

                case AtlasEventsDefinition.ServiceMode:
                    result = self.AsAtlasServiceMode(data) == AtlasEventServiceMode.On;
                    break;

                default:
                    result = false;
                    break;
            }

            return result;
        }

        private static TResult AsAtlasEnumerationStatusForSingleByte<TResult>(this AtlasEventsDefinition self, AtlasEventsDefinition expected, byte[] data)
            where TResult : struct => AsAtlasEnumerationStatusForSingleByteWithFunc<TResult>(self, x => x == expected, data);

        private static TResult AsAtlasEnumerationStatusForSingleByteWithFunc<TResult>(this AtlasEventsDefinition self,
            Expression<Func<AtlasEventsDefinition, bool>> expr, byte[] data)
            where TResult : struct
        {
            var expression = expr.Compile();

            if (!expression(self))
                throw new InvalidOperationException();
            if (data.Length < 1) // sizeof(uint8)
                throw new InvalidOperationException();

            return EnumConverter<TResult>.Convert(data[0]);
        }

        private static TResult AsAtlasClassStatusForSingleByte<TResult>(this AtlasEventsDefinition self, AtlasEventsDefinition expected, byte[] data)
            where TResult : class
        {
            if (self != expected)
                throw new InvalidOperationException();
            if (data.Length < 1) // sizeof(uint8)
                throw new InvalidOperationException();

            return (TResult)Activator.CreateInstance(typeof(TResult), new[] { data[0] });
        }
    }
}
