﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace AST.Infrastructure.Bussiness
{
    public interface ITracingWorkForApplication: IDisposable
    {
        CancellationToken Token { get; }
    }

    public interface IApplicationWorkspace
    {
        event EventHandler<EventArgs> OnCancelAsync;

        bool ProcessOperation { get; }

        ITracingWorkForApplication CreateTracingWork();

        Task CancelAllAsync();
    }
}
