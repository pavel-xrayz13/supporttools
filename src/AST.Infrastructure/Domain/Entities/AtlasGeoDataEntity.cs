﻿using System;

namespace AST.Infrastructure.Domain.Entities
{
    public class AtlasGeoDataEntity
    {
        private readonly int lattitude;
        private readonly int longitude;

        public double Lattitude => ToDecimalValue(lattitude);
        public double Longitude => ToDecimalValue(longitude);
        public DateTime Created { get; set; }

        public AtlasGeoDataEntity(int lattitude, int longitude, DateTime created)
        {
            this.lattitude = lattitude;
            this.longitude = longitude;
            Created = created;
        }

        public static AtlasGeoDataEntity Default() => new AtlasGeoDataEntity(default, default, default);

        public bool IsValid => !IsDefault;

        public bool IsDefault => lattitude == default && longitude == default;

        private static double ToDecimalValue(int value)
        {
            return value * .0000001;
        }
    }
}
