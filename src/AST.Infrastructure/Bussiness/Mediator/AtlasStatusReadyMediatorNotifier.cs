﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;
using AST.Infrastructure.Bussiness.ViewModels;
using AST.Infrastructure.Domain.Entities;
using Autofac;
using MediatR;

namespace AST.Infrastructure.Bussiness.Mediator
{
    public class AtlasStatusReadyNotify : INotification
    {
        public AtlasStatusEntity Entity { get; }

        public AtlasStatusReadyNotify(AtlasStatusEntity entity)
        {
            Entity = entity ?? throw new ArgumentNullException(nameof(entity));
        }
    }

    public class AtlasStatusReadyMediatorNotifier : INotificationHandler<AtlasStatusReadyNotify>
    {
        private readonly IComponentContext context;
        private readonly Dispatcher dispatcher;

        public AtlasStatusReadyMediatorNotifier(IComponentContext context, Dispatcher dispatcher)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.dispatcher = dispatcher;
        }

        public Task Handle(AtlasStatusReadyNotify notification, CancellationToken ct)
        {
            if (!ct.IsCancellationRequested)
            {
                var destinationViewModel = context.Resolve<AtlasStatusViewModel>();
                destinationViewModel.Entity = notification.Entity;
            }
            return Task.CompletedTask;
        }
    }

}
