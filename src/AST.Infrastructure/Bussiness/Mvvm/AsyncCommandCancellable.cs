﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;

namespace AST.Infrastructure.Bussiness.Mvvm
{
    public class AsyncCommandCancellable : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private bool isExecuting;
        private readonly Func<CancellationToken, Task> execute;
        private readonly Action beforeAsyncExecute;
        private readonly Func<bool> canExecute;
        private readonly Dispatcher dispatcher;
        private readonly CancellationTokenSource cts;

        public AsyncCommandCancellable(Dispatcher dispatcher, CancellationTokenSource cts,
            Action beforeAsyncExecute, Func<CancellationToken, Task> execute, Func<bool> canExecute = null)
        {
            this.execute = execute;
            this.canExecute = canExecute;
            this.dispatcher = dispatcher;
            this.cts = cts;
            this.beforeAsyncExecute = beforeAsyncExecute;
        }

        public bool CanExecute() => !isExecuting && (canExecute?.Invoke() ?? true);

        public async Task ExecuteAsync()
        {
            if (CanExecute())
            {
                try
                {
                    isExecuting = true;
                    await execute(cts.Token);
                }
                finally
                {
                    isExecuting = false;
                }
            }

            RaiseCanExecuteChanged();
        }

        public void RaiseCanExecuteChanged()
        {
            dispatcher.Invoke(() =>
            {
                CanExecuteChanged?.Invoke(this, EventArgs.Empty);
            });
        }

        #region ICommand implementations
        bool ICommand.CanExecute(object parameter)
        {
            return CanExecute();
        }

        void ICommand.Execute(object parameter)
        {
            beforeAsyncExecute?.Invoke();
            Task.Run(async () => await ExecuteAsync().ConfigureAwait(true), cts.Token);
        }
        #endregion
    }
}
