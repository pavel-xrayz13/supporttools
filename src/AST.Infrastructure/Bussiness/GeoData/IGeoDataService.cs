﻿using AST.Infrastructure.Domain.Entities;
using System;

namespace AST.Infrastructure.Bussiness.GeoData
{
    public enum GeoDataProvider
    {
        Google,
        Yandex
    }

    public interface IGeoDataService
    {
        Uri PointOnMapUri(GeoDataProvider provider, AtlasGeoDataEntity geoData);
    }
}
