﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace AST.Presentation.Converters
{
    internal abstract class GenericDefaultSemanticsToStringConverter<TType> : IValueConverter
    {
        public object Convert(object value, Type targetType, object defaultValueParameter, CultureInfo culture)
        {
            if (!(value is TType))
                throw new InvalidOperationException($"{typeof(TType)} type expected");

            var typed = (TType)value;
            if (typed.Equals(default(TType)))
                return string.Empty;

            return typed.ToString();
        }

        public virtual object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
