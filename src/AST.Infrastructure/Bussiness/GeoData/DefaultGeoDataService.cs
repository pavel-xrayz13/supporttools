﻿using System;
using System.Globalization;
using AST.Infrastructure.Domain.Entities;

namespace AST.Infrastructure.Bussiness.GeoData
{
    public class DefaultGeoDataService : IGeoDataService
    {
        public Uri PointOnMapUri(GeoDataProvider provider, AtlasGeoDataEntity geoData)
        {
            if (geoData == null)
                throw new ArgumentNullException(nameof(geoData));

            return GenerateFullLinkForProvider(provider, geoData);
        }

        private static Uri GenerateFullLinkForProvider(GeoDataProvider provider, AtlasGeoDataEntity geoData)
        {
            string baseUri = string.Empty;
            string query = string.Empty;

            switch(provider)
            {
                case GeoDataProvider.Google:
                    baseUri = "www.google.com/maps";
                    query = GenerateGoogleMapCoordinatesGet(geoData);
                    break;
                case GeoDataProvider.Yandex:
                    baseUri = "www.yandex.ru/maps";
                    query = GenerateYandexMapCoordinatesGet(geoData);
                    break;
                default:
                    throw new InvalidOperationException("Unknown map provider");
            }

            return new Uri($"https://{baseUri}/{query}");
        }

        private static string GenerateGoogleMapCoordinatesGet(AtlasGeoDataEntity geoData)
        {
            var lattiture = geoData.Lattitude.ToString(CultureInfo.InvariantCulture);
            var longitude = geoData.Longitude.ToString(CultureInfo.InvariantCulture);
            return $"@{lattiture},{longitude},19z";
        }

        private static string GenerateYandexMapCoordinatesGet(AtlasGeoDataEntity geoData)
        {
            var lattiture = geoData.Lattitude.ToString(CultureInfo.InvariantCulture);
            var longitude = geoData.Longitude.ToString(CultureInfo.InvariantCulture);
            return $"?ll={longitude},{lattiture}&pt={longitude},{lattiture}&z=19";
        }
    }
}
