﻿using System.Diagnostics;
using System.Windows.Documents;
using System.Windows.Navigation;

namespace AST.Presentation.Controls
{
    public class NavigatingHyperLink : Hyperlink
    {
        public NavigatingHyperLink()
        {
            RequestNavigate += OnRequestNavigate;
        }

        private void OnRequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }
    }
}
